import numpy as np

def gaussian_quad_1d(nPoints):
    '''
    % Description:
    %   This function contains a library of 1D Gaussian Quadrature
    %   rules to be used in approximating the local stiffness matrix
    %   and load vectors.
    %
    % Outputs:
    %   q    <nPoints x 1 double>   List of sample points
    %   w    <nPoints x 1 double>   List of sample weights
    '''

    # Set quadrature rule
    if nPoints == 1:
        q = np.array([0])
        w = np.array([2])

    elif  nPoints == 2:
        q = np.sqrt(1./3.)*np.array([ -1., 1. ])
        w = np.array([1, 1])

    elif nPoints == 3:
        q = np.sqrt(3./5.)*np.array([ 0., -1., 1.])
        w = 1./9.*np.array([ 8., 5., 5.])

    elif nPoints == 4:
        q = np.array([np.sqrt(3. / 7. - 2./7.*np.sqrt(6./5.)) , -np.sqrt(3. / 7. - 2./7.*np.sqrt(6./5.)) , np.sqrt(3. / 7. + 2./7.*np.sqrt(6./5.)) , -np.sqrt(3. / 7. + 2./7.*np.sqrt(6./5.)) ])
        w = 1. / 36. * np.array([18. + np.sqrt(30.), 18. + np.sqrt(30.), 18. - np.sqrt(30.), 18. - np.sqrt(30.)])

    else:
        print('Unsupported Gaussian Quadrature rule requested')

    return w, q

def gaussian_quad_2d(nPoints):
    w1,q1 = gaussian_quad_1d(nPoints)

    q2x, q2y = np.meshgrid(q1, q1)
    q2 = np.empty((nPoints*nPoints, 2))
    q2[:, 0], q2[:, 1] = np.reshape(q2x, -1), np.reshape(q2y, -1)

    w2 = np.reshape(np.outer(w1, w1) ,-1)

    return w2, q2

def shape_function(r, s, type):
    '''
    2D shape function in [-1,1][-1,1]

    :param r, s: coordinate in the parent element
    :param type: Q1 or Q2
    :param order: 1 or 2, derivative of shape functions
    :return:
    '''


    if type == 'Q1':
        sh = np.empty((4, 3))
        sh[:, 0] = (r - 1.) * (s - 1.) / 4.0, -(r + 1.) * (s - 1.) / 4.0, (r + 1.) * (s + 1.) / 4.0, -(r - 1.) * (
        s + 1.) / 4.0
        sh[:, 1] = (s - 1.) / 4.0, -(s - 1.) / 4.0, (s + 1.) / 4.0, -(s + 1.) / 4.0
        sh[:, 2] = (r - 1.) / 4.0, -(r + 1.) / 4.0, (r + 1.) / 4.0, -(r - 1.) / 4.0
    elif type == 'Q2':
        sh = np.empty((9, 6))
        sh[:, 0] = r * (r - 1) * s * (s - 1) / 4.0, r * (r + 1) * s * (s - 1) / 4.0, r * (r + 1) * s * (
        s + 1) / 4.0, r * (r - 1) * s * (s + 1) / 4.0, \
                         -(r * r - 1) * s * (s - 1.) / 2.0, -r * (r + 1) * (s * s - 1.) / 2.0, -(r * r - 1) * s * (
                         s + 1.) / 2.0, -r * (r - 1) * (s * s - 1.) / 2.0, \
                         (1 - r * r) * (1 - s * s)

        sh[:, 1] = (2 * r - 1) * s * (s - 1) / 4.0, (2 * r + 1) * s * (s - 1) / 4.0, (2 * r + 1) * s * (
            s + 1) / 4.0, (2 * r - 1) * s * (s + 1) / 4.0, \
                         -(2 * r) * s * (s - 1.) / 2.0, -(2 * r + 1) * (s * s - 1.) / 2.0, -(2 * r) * s * (
                             s + 1.) / 2.0, -(2 * r - 1) * (s * s - 1.) / 2.0, \
                         (-2 * r) * (1 - s * s)

        sh[:, 2] = r * (r - 1) * (2 * s - 1) / 4.0, r * (r + 1) * (2 * s - 1) / 4.0, r * (r + 1) * (
        2 * s + 1) / 4.0, r * (r - 1) * (2 * s + 1) / 4.0, \
                         -(r * r - 1) * (2 * s - 1.) / 2.0, -r * (r + 1) * (2 * s) / 2.0, -(r * r - 1) * (
                         2 * s + 1.) / 2.0, -r * (r - 1) * (2 * s) / 2.0, \
                         (1 - r * r) * (-2 * s)

        sh[:, 3] = 2 * s * (s - 1) / 4.0, 2 * s * (s - 1) / 4.0, 2 * s * (
            s + 1) / 4.0, 2 * s * (s + 1) / 4.0, \
                         -2 * s * (s - 1.) / 2.0, -2 * (s * s - 1.) / 2.0, -2 * s * (
                             s + 1.) / 2.0, -2 * (s * s - 1.) / 2.0, \
                         -2 * (1 - s * s)

        sh[:, 4] = (2 * r - 1) * (2 * s - 1) / 4.0, (2 * r + 1) * (2 * s - 1) / 4.0, (2 * r + 1) * (
        2 * s + 1) / 4.0, \
                         (2 * r - 1) * (2 * s + 1) / 4.0, \
                         -(2 * r) * (2 * s - 1.) / 2.0, -(2 * r + 1) * 2 * s / 2.0, -(2 * r) * (2 * s + 1.) / 2.0, \
                         -(2 * r - 1) * 2 * s / 2.0, (-2 * r) * (- 2 * s)

        sh[:, 5] = r * (r - 1) * 2 / 4.0, r * (r + 1) * 2 / 4.0, r * (r + 1) * 2 / 4.0, r * (r - 1) * 2 / 4.0, \
                         -(r * r - 1) * 2 / 2.0, -r * (r + 1) * 2 / 2.0, -(r * r - 1) * 2 / 2.0, -r * (r - 1) * 2 / 2.0, \
                         (1 - r * r) * (-2)

    else:
        print('unknown finite element type')
    return sh

def shape_function_1d(r, type, h):
    '''
    1D shape function in [-1,1]

    :param r: coordinate in the parent element
    :param type: Q1 or Q4
    :param order: 1 or 2, derivative of shape functions
    :return:
    '''


    if type == 'Q1':
        sh = np.empty((2, 2))
        sh[:, 0] = (1 - r)/ 2.0, (r + 1.) / 2.0
        sh[:, 1] = - 1./2.0, 1.0/2.0

    elif type == 'Q4':
        sh = np.empty((4, 3))
        sh[:, 0] = (4 - 6*r + 2*r**3)/8.0, (4 + 6*r - 2*r**3)/8., \
                   h*(r*r - 1)*(r - 1)/8.0, h*(r*r - 1)*(r + 1)/8.0

        sh[:, 1] = (- 3 + 3 * r ** 2) / 4.0, (3 - 3 * r ** 2) / 4., \
                   h * (3*r*r - 2*r - 1)/8.0, h * (3*r*r + 2*r - 1)/ 8.0

        sh[:, 2] = (3 * r) / 2.0, (- 3 * r) / 2.0, \
                   h * (3 * r  - 1) / 4.0, h * (3 * r + 1) / 4.0


    else:
        print('unknown finite element type')
    return sh


def build_shape_function(ngp):
    '''
    shape function values on ngpt gaussian quadrature point

    sh1[ngpt][4][0] :  value of 4 Q1 shape functions
    sh1[ngpt][4][1]: r derivative of 4 Q1 shape functions
    sh1[ngpt][4][2]: s derivative of 4 Q1 shape functions

    sh2[ngpt][9][0] :  value of 9 Q2 shape functions
    sh2[ngpt][9][1]: r derivative of 9 Q2 shape functions
    sh2[ngpt][9][2]: s derivative of 9 Q2 shape functions
    sh2[ngpt][9][3]: rr derivative of 9 Q2 shape functions
    sh2[ngpt][9][4]: rs derivative of 9 Q2 shape functions
    sh2[ngpt][9][5]: ss derivative of 9 Q2 shape functions


    shape function values on nodes
    shn2[nNodesElement][9][0] :  value of 9 Q1 shape functions
    shn2[nNodesElement][9][1]: r derivative of 9 Q1 shape functions
    shn2[nNodesElement][9][2]: s derivative of 9 Q1 shape functions

    shape function values on edge gaussian quadrature point
    0o-----------1o
    she2[4][ngp][4][0] :  value of 2 Q1 shape functions
    she2[4][ngp][4][1]: r derivative of 2 Q1 shape functions
    she2[4][ngp][4][2]: s derivative of 2 Q1 shape functions

    0o----1o----2o
    she2[4][ngp][9][0] :  value of 3 Q2 shape functions
    she2[4][ngp][9][1]: r derivative of 3 Q2 shape functions
    she2[4][ngp][9][2]: s derivative of 3 Q2 shape functions
    she2[4][ngp][9][3]: rr derivative of 3 Q2 shape functions
    she2[4][ngp][9][4]: rs derivative of 3 Q2 shape functions
    she2[4][ngp][9][5]: ss derivative of 3 Q2 shape functions



    :return:
    '''
    nDim = 2
    ngpt = ngp ** nDim
    _, gp_coord_2d = gaussian_quad_2d(ngp)
    _, gp_coord_1d = gaussian_quad_1d(ngp)
    sh1 = np.zeros([ngpt, 2 ** nDim, nDim + 1])
    sh2 = np.zeros([ngpt, 3 ** nDim, nDim + 1 + nDim * (nDim + 1) // 2])
    for igp in range(ngpt):
        r, s = gp_coord_2d[igp, :]
        sh1[igp, :, :] = shape_function(r,s,'Q1')

        sh2[igp, :, :] = shape_function(r,s,'Q2')

    ###########################################################################################################################
    # Shape function evaluated on the nodes
    shn1 = np.zeros([2 ** nDim, 2 ** nDim, nDim + 1])
    shn2 = np.zeros([3 ** nDim, 3 ** nDim, nDim + 1 + nDim * (nDim + 1) // 2])
    coord = np.array([[-1., -1.], [1., -1.], [1., 1.], [-1., 1.], [0., -1.], [1., 0.], [0., 1.], [-1., 0.], [0., 0.]])
    for i in range(2 ** nDim):
        r, s = coord[i, :]
        shn1[i, :, :] = shape_function(r,s,'Q1')

    for i in range(3 ** nDim):
        r, s = coord[i, :]
        shn2[i, :, :] = shape_function(r,s,'Q2')

    #########################################################################################################################
    # Shape function evaluated on the edge gaussian point
    she1 = np.zeros([2 * nDim, ngp, 2 ** nDim, nDim + 1])
    she2 = np.zeros([2 * nDim, ngp, 3 ** nDim, nDim + 1 + nDim * (nDim + 1) // 2])

    for e in range(2 * nDim):
        for igp in range(ngp):
            if e == 0:
                r, s = gp_coord_1d[igp], -1.
            elif e == 1:
                r, s = 1., gp_coord_1d[igp]
            elif e == 2:
                r, s = 1. - gp_coord_1d[igp], 1.
            elif e == 3:
                r, s = -1., 1. - gp_coord_1d[igp]

            she1[e, igp, :, :] = shape_function(r,s,'Q1')
            she2[e, igp, :, :] = shape_function(r,s,'Q2')

    return sh1, sh2, shn1, shn2, she1, she2



def build_beam_shape_function(ngp, h):
    '''
    shape function values on ngp gaussian quadrature point

    sh1[ngpt][2][0] :  value of 2 linear shape functions
    sh1[ngpt][2][1] :  r derivative of 2 linear shape functions


    sh4[ngpt][4][0] :  value of 4 Hermite Q2 shape functions
    sh4[ngpt][4][1]: r derivative of 4 Hermite Q2 shape functions
    sh4[ngpt][4][2]: rr derivative of 4 Hermite Q2 shape functions

    :return:
    '''

    ngpt = ngp
    _, gp_coord_1d = gaussian_quad_1d(ngp)
    sh1 = np.zeros([ngpt, 2, 2])
    sh4 = np.zeros([ngpt, 4, 3])
    for igp in range(ngpt):
        r = gp_coord_1d[igp]
        sh1[igp, :, :] = shape_function_1d(r, 'Q1', h)

        sh4[igp, :, :] = shape_function_1d(r, 'Q4', h)

    return sh1, sh4


if __name__ == '__main__':
    w,q = gaussian_quad_2d(3)