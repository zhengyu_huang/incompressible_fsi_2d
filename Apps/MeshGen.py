import numpy as np

def build_mesh(Nx, Ny, Lx_0, Ly_0, Lx_1, Ly_1, elementOrder = 1):
    nElements = Nx * Ny
    nNodes = (elementOrder * Nx + 1) * (elementOrder * Ny + 1)


    x = np.linspace(Lx_0, Lx_1, elementOrder * Nx + 1)
    y = np.linspace(Ly_0, Ly_1, elementOrder * Ny + 1)

    X, Y = np.meshgrid(x, y)
    Coord = np.empty((2, nNodes))
    Coord[0, :], Coord[1, :] = np.reshape(X, -1), np.reshape(Y, -1)

    return nElements, nNodes, Coord

def build_IEN(Nx, Ny, elementOrder = 1):
    # Build IEN Array
    # construct  element nodes array
    # IEN(i,e) is the global node id of element e's node i
    nNodesElement = (elementOrder + 1)**2
    if elementOrder == 1:
        IEN = np.zeros([nNodesElement, Nx * Ny], dtype='int')
        Npx = Nx + 1
        for iy in range(Ny):
            for ix in range(Nx):
                e = Nx * iy + ix
                M = Npx * iy + ix
                IEN[0, e] = M
                IEN[1, e] = M + 1
                IEN[2, e] = M + 1 + Npx
                IEN[3, e] = M + Npx
    elif elementOrder == 2:
        IEN = np.zeros([nNodesElement, Nx*Ny], dtype='int')
        Npx = 2 * Nx + 1
        for iy in range(Ny):
            for ix in range(Nx):
                e = Nx * iy + ix
                M = 2 * (Npx * iy + ix)
                IEN[0, e] = M
                IEN[1, e] = M + 2
                IEN[2, e] = M + 2 + 2 * Npx
                IEN[3, e] = M + 2 * Npx
                IEN[4, e] = M + 1
                IEN[5, e] = M + 2 + Npx
                IEN[6, e] = M + 1 + 2 * Npx
                IEN[7, e] = M + Npx
                IEN[8, e] = M + 1 + Npx
    return IEN


def build_ID(Nx, Ny, nDoF, EBC, elementOrder, elementType):
    # construct destination array
    # ID(d,n) is the global equation number of node n's dth freedom, -1 means no freedom
    nNodes = (elementOrder * Nx + 1) * (elementOrder * Ny + 1)
    ID = np.zeros([nNodes, nDoF], dtype='int') - 1
    eq_id = 0
    for j in range(nDoF):
        for iy in range(elementOrder * Ny + 1):
            for ix in range(elementOrder * Nx + 1):
                i = ix + iy * (elementOrder * Nx + 1)

                if (EBC[i, j] == 0):
                    if (j == nDoF - 1 and elementType == 'Q2Q1' and (ix % 2 == 1 or iy % 2 == 1)):
                        # For the pressure freedom, set both to -2
                        EBC[i, j] = -2
                        ID[i, j] = -2
                    else:
                        ID[i, j] = eq_id
                        eq_id += 1
    return eq_id, ID

def mesh_gen(problemType,Nx, Ny, ngp = 2):

    NBC = None # Neumann boundary

    if problemType == 'Modified_Cavity_Fluid':
        print("Generating mesh for modified cavity fluid")
        elementOrder, elementType = 2, 'Q2Q1'
        nNodesElement = 9
        nDoF = 3
        nDofsArray = np.array([(elementOrder+1)**2, (elementOrder+1)**2, (1+1)**2], dtype=int)
        Lx_0, Ly_0, Lx_1, Ly_1 = 0., 0., 1., 1.
        nElements, nNodes, Coord = build_mesh(Nx, Ny, Lx_0, Ly_0, Lx_1, Ly_1, elementOrder)
        IEN = build_IEN(Nx, Ny, 2)

        #########################################################
        EBC = np.zeros((nNodes, nDoF), dtype=int)
        g = np.zeros((nNodes, nDoF), dtype=float)
        # Top
        # u velocity is prescribed 
        EBC[2*Ny*(2*Nx + 1): (2*Ny + 1)*(2*Nx + 1), 0] = 2
        # v velocity is fixed
        EBC[2*Ny*(2*Nx + 1): (2*Ny + 1)*(2*Nx + 1), 1] = 1
        g[2*Ny*(2*Nx + 1): (2*Ny + 1)*(2*Nx + 1), 1] = 0.



        # Bottom
        # u,v velocities are prescribed from structure 
        EBC[0: (2 * Nx + 1), 0:2] = 3

        # freeEle = 2 if Nx == 32 else 1
        freeEle = Nx//2
        # Left wall
        # u,v velocities are 0
        EBC[0:(2*Ny - 2*freeEle - 1)* (2*Nx + 1): (2*Nx + 1), 0:2] = 1
        g[0:(2 * Ny - 2*freeEle - 1) * (2 * Nx + 1): (2 * Nx + 1), 0:2] = 0.
        # Right wall
        # u,v velocities are 0
        EBC[2*Nx: 2*Nx + (2 * Ny - 2*freeEle - 1) * (2 * Nx + 1): (2 * Nx + 1), 0:2] = 1
        g[2*Nx: 2*Nx + (2 * Ny - 2*freeEle - 1) * (2 * Nx + 1): (2 * Nx + 1), 0:2] = 0.


        NBC = np.zeros((nElements, 4), dtype=int)

        #todo boundary condition
        bc = 2
        for i in range(freeEle):
            NBC[Nx * (Ny - i - 1), 3] = bc
            NBC[Nx * (Ny - i) - 1, 1] = bc

        # Fix pressrure at top left corner
        EBC[2 * Ny * (2 * Nx + 1), 2] = 1
        g[2 * Ny * (2 * Nx + 1), 2] = 0.


        nEquations, ID = build_ID(Nx, Ny, nDoF, EBC, elementOrder, elementType)


    elif problemType == 'Modified_Cavity_Structure':
        print("Generating mesh for modified cavity structure")
        elementOrder, elementType = 2, 'Q2'
        nNodesElement = 9
        nDoF = 4
        nDofsArray = np.zeros(nDoF, dtype=int) + (elementOrder + 1) ** 2
        Lx_0, Ly_0, Lx_1, Ly_1 = 0., -0.002, 1., 0
        nElements, nNodes, Coord = build_mesh(Nx, Ny, Lx_0, Ly_0, Lx_1, Ly_1, elementOrder)
        IEN = build_IEN(Nx, Ny, elementOrder)

        #########################################################
        EBC = np.zeros((nNodes, nDoF), dtype=int)
        g = np.zeros((nNodes, nDoF), dtype=float)

        # Left
        EBC[0, :] = 1
        g[0, :] = 0.
        EBC[2*Nx + 1, :] = 1
        g[2*Nx + 1, :] = 0.
        EBC[4 * Nx + 2, :] = 1
        g[4 * Nx + 2, :] = 0.
        # Right
        EBC[2*Nx, :] = 1
        g[2*Nx, :] = 0.
        EBC[4 * Nx + 1, :] = 1
        g[4 * Nx + 1, :] = 0.
        EBC[6 * Nx + 2, :] = 1
        g[6 * Nx + 2, :] = 0.
        nDoF = 4
        nEquations, ID = build_ID(Nx, Ny, nDoF, EBC, elementOrder, elementType)


    elif problemType == 'Modified_Cavity_Structure_Q1':
        print("Generating mesh for modified cavity structure")
        elementOrder, elementType = 1, 'Q1'
        nNodesElement = 4
        nDoF = 4
        nDofsArray = np.zeros(nDoF, dtype=int) + (elementOrder + 1) ** 2
        Lx_0, Ly_0, Lx_1, Ly_1 = 0., -0.002, 1., 0
        nElements, nNodes, Coord = build_mesh(Nx, Ny, Lx_0, Ly_0, Lx_1, Ly_1, elementOrder)
        IEN = build_IEN(Nx, Ny, elementOrder)

        #########################################################
        EBC = np.zeros((nNodes, nDoF), dtype=int)
        g = np.zeros((nNodes, nDoF), dtype=float)

        # Left
        EBC[0, :] = 1
        g[0, :] = 0.
        EBC[Nx + 1, :] = 1
        g[Nx + 1, :] = 0.

        # Right
        EBC[Nx, :] = 1
        g[Nx, :] = 0.
        EBC[2 * Nx + 1, :] = 1
        g[2 * Nx + 1, :] = 0.

        nDoF = 4
        nEquations, ID = build_ID(Nx, Ny, nDoF, EBC, elementOrder, elementType)

    elif problemType == 'Modified_Cavity_Beam':
        print("Generating mesh for modified cavity beam")
        elementOrder, elementType = 1, '2-node-beam'
        nNodesElement = 2
        nDoF = 6
        # vel, disp(u_array, w_array, theta)
        nDofsArray = np.array([2,2,2,2,2,2])
        Lx_0, Lx_1 = 0.,  1.
        nElements, nNodes = Nx, Nx + 1
        Coord = np.zeros((2, nNodes))
        Coord[0,:] = np.linspace(0,1.,Nx+1)

        IEN = np.zeros([nNodesElement, Nx * Ny], dtype='int')
        IEN[0, :] = np.arange(0, Nx)
        IEN[1, :] = np.arange(1, Nx + 1)

        #########################################################
        EBC = np.zeros((nNodes, nDoF), dtype=int)
        g = np.zeros((nNodes, nDoF), dtype=float)

        # Left
        EBC[0, :] = 1
        g[0, :] = 0.


        # Right
        EBC[Nx, :] = 1
        g[Nx, :] = 0.



        ID = np.zeros([nNodes, nDoF], dtype='int') - 1
        eq_id = 0
        for j in range(nDoF):
            for ix in range(Nx + 1):
                if (EBC[ix, j] == 0):
                    ID[ix, j] = eq_id
                    eq_id += 1
        nEquations = eq_id


    elif problemType == 'Modified_Cavity_Mesh':
        print("Generating mesh for modified cavity mesh")
        elementOrder, elementType = 2, 'Q2'
        nNodesElement = 9
        nDoF = 4
        nDofsArray = np.zeros(nDoF, dtype=int) + (elementOrder + 1) ** 2
        Lx_0, Ly_0, Lx_1, Ly_1 = 0., 0, 1., 1.0
        nElements, nNodes, Coord = build_mesh(Nx, Ny, Lx_0, Ly_0, Lx_1, Ly_1, elementOrder)
        IEN = build_IEN(Nx, Ny, 2)

        #########################################################
        EBC = np.zeros((nNodes, nDoF), dtype=int)
        g = np.zeros((nNodes, nDoF), dtype=float)

        # Bottom
        EBC[0: (2 * Nx + 1), :] = 3
        # Top
        EBC[2 * Ny * (2 * Nx + 1): (2 * Ny + 1) * (2 * Nx + 1), :] = 1
        g[2 * Ny * (2 * Nx + 1): (2 * Ny + 1) * (2 * Nx + 1), :] = 0.
        # Left
        EBC[0:(2 * Ny + 1) * (2 * Nx + 1): (2 * Nx + 1), :] = 1
        g[0:(2 * Ny + 1) * (2 * Nx + 1): (2 * Nx + 1), :] = 0.
        # Right
        EBC[2 * Nx: 2 * Nx + (2 * Ny + 1) * (2 * Nx + 1): (2 * Nx + 1), :] = 1
        g[2 * Nx: 2 * Nx + (2 * Ny + 1) * (2 * Nx + 1): (2 * Nx + 1), :] = 0.



        nDoF = 4
        nEquations, ID = build_ID(Nx, Ny, nDoF, EBC, elementOrder, elementType)

    elif problemType == 'Cavity':
        print("Generating mesh for cavity")
        elementOrder, elementType = 2, 'Q2Q1'
        nNodesElement = 9
        nDoF = 3
        nDofsArray = np.array([(elementOrder+1)**2, (elementOrder+1)**2, (1+1)**2], dtype=int)
        Lx_0, Ly_0, Lx_1, Ly_1 = 0., 0., 1., 1.
        nElements, nNodes, Coord = build_mesh(Nx, Ny, Lx_0, Ly_0, Lx_1, Ly_1, elementOrder)
        IEN = build_IEN(Nx, Ny, 2)

        #########################################################
        EBC = np.zeros((nNodes, nDoF), dtype=int)
        g = np.zeros((nNodes, nDoF), dtype=float)
        #Top
        EBC[2*Ny*(2*Nx + 1): (2*Ny + 1)*(2*Nx + 1), 0] = 1
        g[2*Ny*(2*Nx + 1): (2*Ny + 1)*(2*Nx + 1), 0] = 1.

        EBC[2 * Ny * (2 * Nx + 1): (2 * Ny + 1) * (2 * Nx + 1), 1] = 1
        g[2 * Ny * (2 * Nx + 1): (2 * Ny + 1) * (2 * Nx + 1), 1] = 0.

        #Fix the pressure of the top-left corner
        EBC[2 * Ny * (2 * Nx + 1), 2] = 1
        g[2 * Ny * (2 * Nx + 1), 2] = 0.


        #Left
        EBC[0:2*Ny* (2*Nx + 1): (2*Nx + 1), 0:2] = 1
        g[0:2*Ny * (2 * Nx + 1): (2 * Nx + 1), 0:2] = 0.
        #Right
        EBC[2*Nx: 2*Nx + 2 * Ny * (2 * Nx + 1): (2 * Nx + 1), 0:2] = 1
        g[2*Nx: 2*Nx + 2 * Ny * (2 * Nx + 1): (2 * Nx + 1), 0:2] = 0.
        # Bottom
        EBC[0: (2 * Nx + 1), 0:2] = 1
        g[0: (2 * Nx + 1), 0:2] = 0

        nEquations, ID = build_ID(Nx, Ny, nDoF, EBC, elementOrder, elementType)

    elif problemType == 'Plate_Bending':
        print("Generating mesh for Plate Bending")
        elementOrder, elementType = 1, 'Q1'
        nNodesElement = 4
        nDoF = 4
        nDofsArray = np.array([(elementOrder+1)**2, (elementOrder+1)**2, (elementOrder+1)**2, (elementOrder+1)**2], dtype=int)
        Lx_0, Ly_0, Lx_1, Ly_1 = 0., 0., 1., 1.
        nElements, nNodes, Coord = build_mesh(Nx, Ny, Lx_0, Ly_0, Lx_1, Ly_1, elementOrder)
        IEN = build_IEN(Nx, Ny, elementOrder)

        #########################################################
        EBC = np.zeros((nNodes, nDoF), dtype=int)
        g = np.zeros((nNodes, nDoF), dtype=float)

        #Left
        EBC[0:(elementOrder*Ny + 1)* (elementOrder * Nx + 1): (elementOrder * Nx + 1), :] = 1
        g[0:(elementOrder*Ny+1) *  (elementOrder * Nx + 1): (elementOrder * Nx + 1), :] = 0.


        nEquations, ID = build_ID(Nx, Ny, nDoF, EBC, elementOrder, elementType)

    elif problemType == 'Taylor_Green':
        print("Generating mesh for Taylor Green Vortex")
        elementOrder, elementType = 2, 'Q2Q1'
        nNodesElement = 4
        nDoF = 3
        nDofsArray = np.array([(elementOrder+1)**2, (elementOrder+1)**2, (elementOrder)**2], dtype=int)
        Lx_0, Ly_0, Lx_1, Ly_1 = 0., 0., 2*np.pi, 2*np.pi
        nElements, nNodes, Coord = build_mesh(Nx, Ny, Lx_0, Ly_0, Lx_1, Ly_1, elementOrder)
        IEN = build_IEN(Nx, Ny, 2)

        #########################################################
        EBC = np.zeros((nNodes, nDoF), dtype=int)
        g = np.zeros((nNodes, nDoF), dtype=float)

        #Dirichlet for velocity boundary conditions
        EBC[0:2*Nx+1, 0:2] = 2
        EBC[2 * Ny * (2 * Nx + 1): (2 * Nx + 1)*(2 * Ny + 1), 0:2] = 2
        EBC[0:(2 * Ny + 1) * (2 * Nx + 1): (2 * Nx + 1), 0:2] = 2
        EBC[2 * Nx:(2 * Ny + 1) * (2 * Nx + 1) + 2 * Nx: (2 * Nx + 1), 0:2] = 2

        #Dirichlet for pressure boundary conditions at only one point
        EBC[0, 2] = 2


        nEquations, ID = build_ID(Nx, Ny, nDoF, EBC, elementOrder, elementType)

    return  Nx, Ny, IEN, EBC, g, NBC,  ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray,nElements, nNodes, nEquations

if __name__ == '__main__':
    Nx, Ny, IEN, EBC, g, ID, Coord, problemType, ngp, nNodesElement, nDofsArray, nEquations = mesh_gen('Modified_Cavity_Structure', 32, 32)
    Nx, Ny, IEN, EBC, g, ID, Coord, problemType, ngp, nNodesElement, nDofsArray, nEquations = mesh_gen('Modified_Cavity_Fluid',32,1)