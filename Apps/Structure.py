import numpy as np
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as plt
import copy

from App import App
import ConstitutiveLaw
from MeshGen import mesh_gen

class Structure(App):
    '''
    This class is the 2D linear structure
    '''
    def __init__(self, Nx, Ny, IEN, EBC, g, NBC,  ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations, rho, E, nu):
        App.__init__(self, Nx, Ny, IEN, EBC, g,  NBC, ID, Coord, problemType, elementType,  ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations, rho)

        self.name = 'LinearStructure'
        self.rho = rho
        self.E = E
        self.nu = nu

        self.matlaw = 'PlaneStrain'


    def set_matlaw(self, matlaw):
        self.matlaw = matlaw

    def get_full_field(self, U, C = None):
        '''
        :param U: Solution vector 1 by nEquations
        :param C: DirichletNodes velocity and displacement vector

        :return U_all: velocity and displacement of all nodes, nNodes by 4 array
        '''

        nNodes = self.nNodes
        nDof = self.nDof
        EBC, g = self.EBC, self.g
        ID = self.ID

        U_all = np.zeros((nNodes, nDof))
        I = (EBC == 0)
        U_all[I] = U[ID[I]]


        DirichletNodes = self.DirichletNodes
        if (C is not None) and (DirichletNodes is not None):
            nb = len(DirichletNodes)
            U_all[DirichletNodes,:] = np.reshape(C, (nb, nDof) ,order='F')

        I = (EBC == 1)
        U_all[I] = g[I]

        #todo time-dependent Dirichlet boundary condition

        return U_all

    def get_local_state(self, e, U_all, C):
        '''
        :param e: element number
        :param U: velocity and displacement, nNodes by 4
        :param C: placeholder
        :return: U_e: local velocity and displacement, 4n array
                 X0_e: undeformed coordinate 2 by n
        '''

        IEN = self.IEN
        Coord = self.Coord
        U_e = np.reshape(U_all[IEN[:, e], :], -1, order='F')
        X0_e = Coord[:, IEN[:,e]]
        return U_e, X0_e


    def local_mass(self, e):
        '''
        :param e: element number
        :return: lM: local mass matrix, nDofsElement by nDofsElement
        '''
        sh = self.sh2 if self.elementType == 'Q2' else self.sh1
        n1 = self.nDofsArray[0]
        rho = self.rho

        ngpt = self.ngpt
        gpWeights2D = self.gpWeights2D

        nDofsElement = np.sum(self.nDofsArray)

        X0_e = self.Coord[:, self.IEN[:, e]]

        lM = np.zeros([nDofsElement, nDofsElement])

        for igp in range(ngpt):
            # map from reference domain (r,s) -> (X,Y), the derivatives at the gaussian point
            J = np.dot(X0_e, sh[igp, :, 1:3])
            Jdet = J[0, 0] * J[1, 1] - J[0, 1] * J[1, 0]

            # Local K and L
            mul = gpWeights2D[igp] * Jdet
            llM = np.outer(sh[igp, :, 0], mul * sh[igp, :, 0])

            lM[0:n1,           0:n1] += rho * llM
            lM[n1:2*n1,     n1:2*n1] += rho * llM

        lM[2*n1:, 2*n1:] = np.identity(2*n1)
        return lM

    def assembly(self, U, C):
        '''
        M\dot{U} = L(U) + F(U)
        dL/d(U) = K
        :param U: state variables, 1d array
        :param C: coupling term, nodal force on the Neumann nodes
        :return: K, L
        '''

        ID = self.ID

        K, L = super(Structure, self).assembly(U,C)

        wettedNodes = self.NeumannNodes
        if wettedNodes is not None:
            F = C


            PI_r = ID[wettedNodes, 0:self.nDim].flatten('F')
            PI_c = ID[wettedNodes, :].flatten('F')
            I_r, I_c = (PI_r >= 0), (PI_c >= 0)
            PI_r, PI_c = PI_r[I_r], PI_c[I_c]

            L[PI_r] += F.flatten('F')[I_r]


        return K, L




    def local_assembly(self, e, U_e, X0_e):
        '''
        assemble the RHS and its derivative
        :param U_e: 1 by velocity and deformation
        :param X0_e: 2 by nNodesElement, undeformed coordinate
        :return:
        '''


        sh = self.sh2 if self.elementType == 'Q2' else self.sh1

        n1 = self.nDofsArray[0]

        ngpt = self.ngpt
        gpWeights2D = self.gpWeights2D


        nDofsElement = np.sum(self.nDofsArray)
        lK, lL = np.zeros([nDofsElement, nDofsElement]), np.zeros(nDofsElement)

        #displacement
        u_e, v_e, = U_e[2*n1: 3*n1], U_e[3*n1: 4*n1]

        for igp in range(ngpt):
            # map from reference domain (r,s) -> (X,Y), the derivatives at the gaussian point
            J = np.dot(X0_e, sh[igp, :, 1:3])
            Jdet = J[0, 0] * J[1, 1] - J[0, 1] * J[1, 0]
            Jinv = np.array([[J[1, 1] / Jdet, -J[0, 1] / Jdet], [-J[1, 0] / Jdet, J[0, 0] / Jdet]])

            # x, y derivatives of shape functions
            sh_xy = np.dot(sh[igp, :, 1:3], Jinv)

            # deformation gradient
            u_x, u_y = np.dot(u_e, sh_xy[:, :])
            v_x, v_y = np.dot(v_e, sh_xy[:, :])

            E = np.array([u_x, v_y, (u_y + v_x)])
            S, dS_dE = ConstitutiveLaw.constitutive_law(E, self.matlaw, [self.E, self.nu])

            '''
            with respect to derivative dS_dE, dE_dF
            S = [S11, S22,  S12]
            E = [E11, E22, 2E12]
            '''

            # S is 2 by 2 and dS is 2 by 2 by nDoF
            dE = np.zeros((2*n1, 3))
            dE[0:n1, 0] = sh_xy[:, 0]
            dE[n1:2*n1, 1] = sh_xy[:, 1]
            dE[0:n1, 2], dE[n1:2*n1, 2] = sh_xy[:, 1], sh_xy[:, 0]


            # Local K and L
            mul = gpWeights2D[igp] * Jdet
            # velocity update
            lL[0:2*n1] -= np.dot(dE, S) *mul
            lK[0:2*n1, 2*n1:] -= np.dot(np.dot(dE, dS_dE), dE.T) * mul


        # todo displacement update
        lL[2*n1:] = U_e[0:2*n1]
        lK[2*n1:, 0:2*n1] = np.identity(2*n1)

        return lK, lL



    def get_Qm(self, U, C):
        '''
        :param U: velocity and displacement, nEquations array
        :param C: Dirichlet nodes velocity and displacement, 1d array
        :return: U_all: mesh velocity and displacement vector, 1d array
        '''

        U_all = self.get_full_field(U, C)
        return np.reshape(U_all, -1, order = 'F')

    def get_Qb(self, U):
        '''

        :param U: velocity and displacement , nEquations array
        :return: Neumann Nodes, velocity and displacement, 1d array
        '''
        nDof = self.nDof
        U_all = self.get_full_field(U)
        wettedNodes = self.NeumannNodes #global node id
        nb = len(wettedNodes)
        Qb = np.empty(self.nDof * nb)
        for i in range(nDof):
            Qb[nb*i:nb*(i+1)] = U_all[wettedNodes, i]
        return Qb


    def visualize(self, U, C = None):
        '''
        :param U:  fluid u,v,p array
        :param C:  mesh velocity and mesh displacement
        :return: plot the deformed shape
        '''

        U_all = self.get_full_field(U,C)

        X, Y = self.Coord[0, :], self.Coord[1, :]

        plt.scatter(X, Y, s=2, c='r', label='Initial')
        plt.scatter(X + U_all[:, 2], Y + U_all[:, 3], s=2, c='b', label='Final')

        plt.show()

    def Newmark(self, t_n, dt, U_n, a_n, C, alpham, alphaf):
        '''
        Generalized alpha method for solving M\dotdot{u} = L(u) + F
        a, v, u are acceleration, velocity and displacement

        u_{n+1} = u_n + dtv_n + dt^2/2 ((1 - 2\beta)a_n + 2\beta a_{n+1})
        v_{n+1} = v_n + dt((1 - gamma)a_n + gamma a_{n+1})
        beta = (1 - alpham + alphaf)**2/4.0
        gamma = 1./2. - alpham + alphaf

        Forster, Christiane, Wolfgang A. Wall, and Ekkehard Ramm. "Artificial added mass instabilities in sequential staggered coupling of nonlinear structures and incompressible viscous flows."
        Computer methods in applied mechanics and engineering 196.7 (2007): 1278-1293.
        '''
        
        # active nodes number :nNodes
        nNodes = self.nEquations//4

        v_n, u_n = U_n[0:2*nNodes], U_n[2*nNodes:4*nNodes]

        beta, gamma = (1 - alpham + alphaf)**2/4.0, 1./2. - alpham + alphaf

        M = self.get_mass()[0:2*nNodes, 0:2*nNodes]


        # Newton solve for a_{n+1}
        a_np = copy.copy(a_n)
        NewtonIterstep, NewtonConverge = 0, False
        eps, MaxIterstep = 1.0e-8, 100
        while not NewtonConverge:

            # validate gradient
            # app.check_derivative(u_n)

            NewtonIterstep = NewtonIterstep + 1
            u_np = u_n + dt * v_n + dt * dt / 2.0 * ((1 - 2 * beta) * a_n + 2 * beta * a_np)
            v_np = v_n + dt * ((1 - gamma) * a_n + gamma * a_np)


            a_nh = (a_n + a_np) / 2.0
            v_nh = (v_n + v_np) / 2.0
            u_nh = (u_n + u_np) / 2.0


            K_nh, L_nh = self.assembly(np.concatenate((v_nh, u_nh)), C)

            K_nh = K_nh[0:2*nNodes, 2*nNodes:4*nNodes]

            res = M.dot(a_nh)  - L_nh[0:2*nNodes]

            A = M / 2.0 - K_nh * dt * dt * beta / 2.0

            delta_a_np = spsolve(A, res)

            a_np -= delta_a_np

            print("||F|| is ", np.linalg.norm(res))
            if (np.linalg.norm(res) < eps or NewtonIterstep > MaxIterstep or self.name == 'LinearStructure'):
                if NewtonIterstep > MaxIterstep:
                    print('Newton iteration cannot converge')
                NewtonConverge = True

        v_np = v_n + dt * ((1 - gamma) * a_n + gamma * a_np)
        u_np = u_n + dt * v_n + dt * dt / 2.0 * ((1 - 2 * beta) * a_n + 2 * beta * a_np)

        return np.concatenate((v_np,u_np)), a_np

if __name__ == '__main__':
    rho = 1.
    E = 1.
    nu = 0.0
    Nx, Ny = 1,1
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen('Modified_Cavity_Structure', Nx, Ny)
    app1 = Structure(Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations, rho, E, nu)
    np.random.seed(101)
    Ue = np.random.normal(2.0, 0.2, np.sum(nDofsArray))
    X_e = np.random.normal(2.0, 0.2, (2,9))

    unit = np.random.normal(1.0, 0.1, np.sum(nDofsArray))
    eps = 1.e-4
    Ue_p = Ue + eps*unit
    Ue_m = Ue - eps*unit



    lK,   lL   = app1.local_assembly(0, Ue, X_e)
    lK_p, lL_p = app1.local_assembly(0, Ue_p, X_e)
    lK_m, lL_m = app1.local_assembly(0, Ue_m, X_e)


    unit = (Ue_p - Ue_m)/(2.*eps)

    error = (lL_p - lL_m)/(2*eps) - np.dot(lK,unit)

    print(error)

