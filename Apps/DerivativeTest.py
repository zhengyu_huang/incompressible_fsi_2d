import numpy as np
from App import App
# from NonlinearStructure import NonlinearStructure
from Structure import Structure
from IncompressibleFluid import IncompressibleFluid
from ConstitutiveLaw import constitutive_law
from MeshGen import  mesh_gen
np.random.seed(101)

def IncompressibleFluidTest():
    rho = 1.
    nu = 0.01
    Nx, Ny = 32,32
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen('Modified_Cavity_Fluid', Nx,
                                                                                           Ny)
    ngp = 3
    app0 = IncompressibleFluid(Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations, rho, nu)


    wettedNodesF = np.arange(0, 2 * Nx + 1)
    wettedEdgesF = np.zeros((Nx, 2))
    wettedEdgesF[:, 0] = np.arange(Nx)
    app0.set_wetted_surface(wettedNodesF, wettedEdgesF)


    U = np.random.normal(2.0, 0.2, app0.nEquations)
    C = np.random.normal(1.0, 0.2, 4 * app0.nNodes)

    # Smooth mesh motion
    # C[0:nNodes] = 0.
    # C[nNodes:2*nNodes] = Coord[0,:] * (1 - Coord[0,:]) * (1 - Coord[1, :])
    # C[2*nNodes:3*nNodes] = 0.
    # C[3*nNodes:4*nNodes] = Coord[0,:] * (1 - Coord[0,:]) * (1 - Coord[1, :])

    unit = np.random.normal(1.0, 0.1, app0.nEquations)
    # unit[9:] = 0.0
    eps = 1.e-4
    U_p = U + eps * unit
    U_m = U - eps * unit

    K, L = app0.assembly(U, C)
    K_p, L_p = app0.assembly(U_p, C)
    K_m, L_m = app0.assembly(U_m, C)

    error = (L_p - L_m) / (2 * eps) - K.dot(unit)
    error /= L
    print(error, np.linalg.norm(error), np.linalg.norm(error, np.inf))

def NonlinearStructureTest():
    print('Derivative test, the large aspect ratio reduces the accuracy')
    rho = 1.
    E = 1.
    nu = 0.0
    Nx, Ny = 32,1
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen('Modified_Cavity_Structure', Nx, Ny)
    app1 = NonlinearStructure(Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations, rho, E, nu)

    wettedNodesS = np.arange(4 * Nx + 2, 6 * Nx + 3)

    app1.set_NeumannNodes(wettedNodesS)


    U = np.random.normal(2.0, 0.2, app1.nEquations)

    C = np.random.normal(2.0, 0.2, 2*len(wettedNodesS)) # force on the wetted nodes

    unit = np.random.normal(1.0, 0.1, app1.nEquations)
    # unit[9:] = 0.0
    eps = 1.e-4
    U_p = U + eps * unit
    U_m = U - eps * unit

    K, L = app1.assembly(U, C)
    K_p, L_p = app1.assembly(U_p, C)
    K_m, L_m = app1.assembly(U_m, C)

    error = (L_p - L_m) / (2 * eps) - K.dot(unit)
    error /= L
    print(error, np.linalg.norm(error))

def LinearStructureTest():
    rho = 1.
    E = 1.
    nu = 0.0
    Nx,Ny = 1, 1
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen('Modified_Cavity_Mesh', Nx,Ny)
    app2 = Structure(Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations, rho, E, nu)

    wettedNodesM = np.arange(0, 2 * Nx + 1)
    app2.set_DirichletNodes(wettedNodesM)

    U = np.random.normal(2.0, 0.2, app2.nEquations)

    C = np.random.normal(2.0, 0.2, 4*len(wettedNodesM))  # velocity and displacement of wetted nodes

    unit = np.random.normal(1.0, 0.1, app2.nEquations)
    # unit[9:] = 0.0
    eps = 1.e-4
    U_p = U + eps * unit
    U_m = U - eps * unit

    K, L = app2.assembly(U, C)
    K_p, L_p = app2.assembly(U_p, C)
    K_m, L_m = app2.assembly(U_m, C)


    error = (L_p - L_m) / (2 * eps) - K.dot(unit)
    print(error, np.linalg.norm(error), np.linalg.norm(error, np.inf))

if __name__ == '__main__':
    #NonlinearStructureTest()

    #LinearStructureTest()

    IncompressibleFluidTest()