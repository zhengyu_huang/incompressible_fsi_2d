import numpy as np
import scipy as sp
from scipy.sparse import csr_matrix
import QuadratureRule

class App:
    def __init__(self, Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations, rho):
        self.Nx, self.Ny = Nx, Ny
        self.nDim = nDim = 2
        self.nDof = len(nDofsArray)
        self.ngp = ngp
        self.ngpt = ngp**nDim
        self.gpWeights2D, self.gpCoord2D = QuadratureRule.gaussian_quad_2d(ngp)
        self.gpWeights1D, self.gpCoord1D = QuadratureRule.gaussian_quad_1d(ngp)
        self.nNodesElement = nNodesElement
        self.nElements = nElements
        self.nNodes = nNodes
        self.nDofsArray = nDofsArray
        self.sh1, self.sh2, self.shn1, self.shn2, self.she1, self.she2 = QuadratureRule.build_shape_function(ngp)
        self.IEN = IEN
        self.EBC, self.g = EBC, g
        self.NBC = NBC
        self.nEquations = nEquations
        self.mesh_data_processing(Nx, Ny, IEN, EBC, ID, Coord, problemType)
        self.t = 0.0
        self.elementType = elementType
        # these nodes have velocity, displacement or Force updated at each time step
        self.DirichletNodes = None
        self.NeumannNodes = None
        self.rho = rho
        self.mass_matrix()

    def set_time(self, t):
        self.t = t

    def set_DirichletNodes(self, nodes):
        self.DirichletNodes = nodes

    def set_NeumannNodes(self, nodes):
        self.NeumannNodes = nodes

    def mesh_data_processing(self, Nx, Ny, IEN, EBC, ID, Coord, problemType):
        '''
        nElements: number of elements

        IEN  <nNodesElement x nElements int>  Array of global node numbers,
             IEN(i,e) is the global node id of element e's node i

        ID : <nNodes x nDoF int> Array of global equation numbers, destination array (ID)
            ID(d,n) is the global equation number of node n's d freedom
            -1 means essential boundary condition,
            -2 empty freedom in the pressure, due to mixed finite element


        EBC             <nNodes x nDoF int> EBC = 1 if the node's freedom is on Essential B.C.


        LM              <nNodesElement*nDoF x nElements int> Array of global eq. numbers, location matrix (LM)
                                                     LM(d,e) is the global equation number of element e's d th freedom
        nEquations: number of equations, u..., v..., p...

        Remark, all freedom, first u and then v and finally p
        Q1 element  3    2       Q2     3   6   2
                                        7   8   5
                    0    1              0   4   1
        '''

        self.Nx, self.Ny = Nx, Ny
        self.nElements = Nx * Ny
        self.Coord = Coord
        self.problemType = problemType
        self.EBC = EBC
        self.ID = ID


        # LM(d,e) is the global equation number of element e's d th freedom
        nDofsArray = self.nDofsArray
        nDofsElement = np.sum(nDofsArray)
        LM = np.zeros([nDofsElement, self.nElements], dtype='int')
        for i in range(self.nDof):
            for j in range(nDofsArray[i]):
                for k in range(self.nElements):
    
                    LM[np.sum(nDofsArray[0:i]) + j, k] = ID[self.IEN[j, k], i]

        self.LM = LM

    def get_full_field(self, U, C):

        print('virtual function')

    def get_local_state(self, e, U_all, C):
        '''
        :param e:
        :param U:
        :param C:
        :param t:
        :return: Ue, Ce
        '''

        print('virtual function')

    def local_assembly(self, e, Ue, Ce):
        '''
        :param Ue: vector of nDoFsElements
        :param Ce: coupling term
        :return:
        '''

        print('virtual function')

    def get_mass(self):
        return self.M

    def mass_matrix(self, lumped_or_not = True):
        '''
        Sparse matrix version csr_matrix
        M\dot{U} = L(U) + F
        dL/d(U) = K
        :return: K, L, F
        '''
        nElements = self.nElements
        nEquations = self.nEquations
        LM = self.LM

        row, col, data = [], [], []

        M_lumped = np.zeros(nEquations)

        for e in range(nElements):

            lM = self.local_mass(e)

            # Step 3b: Get Global equation numbers
            PI = LM[:, e]

            # Step 3c: Eliminate Essential DOFs
            I = (PI >= 0)
            PI = PI[I]

            # Step 3d: Insert k_e, f_e, f_g, f_h
            lcol, lrow = np.meshgrid(PI, PI)

            row.extend(np.reshape(lrow, -1))
            col.extend(np.reshape(lcol, -1))
            data.extend(np.reshape(lM[np.ix_(I, I)], -1))

            M_lumped[PI] += np.sum(lM, axis=1)[I]


        self.row, self. col = row, col

        if lumped_or_not:
            self.M = csr_matrix((M_lumped, (np.arange(nEquations), np.arange(nEquations))),
                                   shape=(nEquations, nEquations), dtype=float)
        else:
            self.M = csr_matrix((data, (row, col)), shape=(nEquations, nEquations), dtype=float)


    def local_mass(self, e):
        '''
        :param X_e: local coordinate
        :return:
        '''

        print('virtual function')

    def assembly(self, U, C):
        '''
        M\dot{U} = L(U) + F
        dL/d(U) = K
        :param U: state variables, 1d array
        :param C: coupling term, 1d array
        :return: K, L
        '''
        nElements = self.nElements
        nEquations = self.nEquations
        LM = self.LM
        L = np.zeros((nEquations))
        NBC = self.NBC
        row, col, data = self.row, self.col, []


        U_all = self.get_full_field(U, C)

        for e in range(nElements):

            Ue, Xe = self.get_local_state(e, U_all, C)

            lK, lL = self.local_assembly(e, Ue, Xe)
            
            for id in range(4):
                if (NBC is not None) and (NBC[e, id] == 2):
                    continue # free boundary condition
                    # lK_bc, lL_bc = self.local_free_bc_assembly(e, Ue, Xe, id) 
                    # lK += lK_bc
                    # lL += lL_bc
                
            # Step 3b: Get Global equation numbers
            PI = LM[:, e]

            # Step 3c: Eliminate Essential DOFs
            I = (PI >= 0)
            PI = PI[I]

            # Step 3d: Assemble L

            L[PI] += lL[I]

            # Step 3e: Assemble K
            data.extend(np.reshape(lK[np.ix_(I, I)], -1))


        K = csr_matrix((data, (row, col)), shape=(nEquations, nEquations), dtype=float)

        return K, L

    def visualize_helper(self, U):
        '''

        :param U: Solution vector 1 by nEquations
        :param Coord: Coordinate 2 by nNodes
        :param dofId:
        :return:
        '''
        nNodes = self.nNodes
        nDof = self.nDof
        EBC, g = self.EBC, self.g
        ID = self.ID

        U_all = np.zeros((nNodes, nDof))
        I = (EBC == 0)
        U_all[I] = U[ID[I]]
        U_all[~I] = g[~I]

        return U_all


    def visualize(self, U, C, dofId):
        print('virtual function')



