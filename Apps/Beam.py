import numpy as np
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as plt
import copy

from MeshGen import mesh_gen
import QuadratureRule

class Beam:
    '''
    This class is the 1D nonlinear beam elements in 2D space
    De Borst, Rene, et al. Nonlinear finite element analysis of solids and structures. John Wiley & Sons, 2012. p 308
    '''
    def __init__(self, Nx,  IEN, EBC, g, NBC,  ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations, rho, E, A):
        self.Nx = Nx
        self.nDim = 2
        self.nDof = len(nDofsArray)
        self.ngp = ngp
        self.gpWeights1D, self.gpCoord1D = QuadratureRule.gaussian_quad_1d(ngp)
        self.nNodesElement = nNodesElement
        self.nElements = nElements
        self.nNodes = nNodes
        self.nDofsArray = nDofsArray
        self.h = Coord[0,1] - Coord[0,0]
        self.sh1, self.sh4 = QuadratureRule.build_beam_shape_function(ngp, self.h)
        self.IEN = IEN
        self.EBC, self.g = EBC, g
        self.NBC = NBC
        self.nEquations = nEquations

        self.t = 0.0
        self.elementType = elementType
        # these nodes have velocity, displacement or Force updated at each time step
        self.DirichletNodes = None
        self.NeumannNodes = None
        self.rho = rho

        self.name = 'Beam'
        self.rho = rho
        self.E = E
        self.A = A
        self.I = A**3/12.0
        self.matlaw = 'Linear'
        self.mesh_data_processing(EBC, ID, Coord, problemType)
        self.mass_matrix()


    def mesh_data_processing(self, EBC, ID, Coord, problemType):
        '''
        nElements: number of elements

        IEN  <nNodesElement x nElements int>  Array of global node numbers,
             IEN(i,e) is the global node id of element e's node i

        ID : <nNodes x nDoF int> Array of global equation numbers, destination array (ID)
            ID(d,n) is the global equation number of node n's d freedom
            -1 means essential boundary condition,


        EBC             <nNodes x nDoF int> EBC = 1 if the node's freedom is on Essential B.C.


        LM              <nNodesElement*nDoF x nElements int> Array of global eq. numbers, location matrix (LM)
                                                     LM(d,e) is the global equation number of element e's d th freedom
        nEquations: number of equations, u..., v..., p...

        Remark, all freedom, first u and then v and finally p
        Q1 element  3    2       Q2     3   6   2
                                        7   8   5
                    0    1              0   4   1
        '''


        self.Coord = Coord
        self.problemType = problemType
        self.EBC = EBC
        self.ID = ID


        # LM(d,e) is the global equation number of element e's d th freedom
        nDofsArray = self.nDofsArray
        nDofsElement = np.sum(nDofsArray)
        LM = np.zeros([nDofsElement, self.nElements], dtype='int')
        for i in range(self.nDof):
            for j in range(nDofsArray[i]):
                for k in range(self.nElements):
                    LM[np.sum(nDofsArray[0:i]) + j, k] = ID[self.IEN[j, k], i]

        self.LM = LM

    def set_time(self, t):
        self.t = t

    def set_wettedNodes(self, wettedNodesS):
        '''

        :param wettedNodesS: list of pair(ele, xi), these are matched nodes
        :return: transfer matrix
        '''
        self.wettedNodes = wettedNodesS
        nNodes = self.nNodes
        nDof, nDim = self.nDof, self.nDim
        IEN = self.IEN
        n_d = self.nNodes * self.nDof//2
        h = self.h

        n_w = len(wettedNodesS)
        matcher_trans_matrix = np.zeros((n_w*nDim, n_d))
        for i in range(n_w):
            e, r = wettedNodesS[i]
            n1, n2 = IEN[:nDof//2, e]
            matcher_trans_matrix[i, [n1, n2]] = (1 - r)/ 2.0, (r + 1.) / 2.0 #x displacement
            matcher_trans_matrix[i + n_w, [n1 + nNodes, n2 + nNodes, n1 + 2*nNodes, n2 + 2*nNodes]] = \
                (4 - 6*r + 2*r**3)/8.0, (4 + 6*r - 2*r**3)/8., h*(r*r - 1)*(r - 1)/8.0, h*(r*r - 1)*(r + 1)/8.0  #y displacement

        self.matcher_trans_matrix = matcher_trans_matrix

    def set_matlaw(self, matlaw):
        self.matlaw = matlaw

    def get_full_field(self, U):
        '''
        :param U: Solution vector 1 by nEquations
        :param C: DirichletNodes velocity and displacement vector

        :return U_all: velocity and displacement of all nodes, nNodes by nDoF array
        '''

        nNodes = self.nNodes
        nDof = self.nDof
        EBC, g = self.EBC, self.g
        ID = self.ID

        U_all = np.zeros((nNodes, nDof))
        I = (EBC == 0)
        U_all[I] = U[ID[I]]

        I = (EBC == 1)
        U_all[I] = g[I]

        #todo time-dependent Dirichlet boundary condition

        return U_all

    def get_local_state(self, e, U_all, C):
        '''
        :param e: element number
        :param U: velocity and displacement, nNodes by 4
        :param C: placeholder
        :return: U_e: local velocity and displacement, 4n array
                 X0_e: undeformed coordinate 2 by n
        '''

        IEN = self.IEN
        Coord = self.Coord
        U_e = np.reshape(U_all[IEN[:, e], :], -1, order='F')
        X0_e = Coord[:, IEN[:,e]]
        return U_e, X0_e


    def local_mass(self, e):
        '''
        :param e: element number
        :return: lM: local mass matrix, nDofsElement by nDofsElement
        '''
        sh1, sh4 = self.sh1, self.sh4
        n1, n2 = 2, 4
        A, I = self.A, self.I
        rho = self.rho

        ngp = self.ngp
        gpWeights1D = self.gpWeights1D

        nDofsElement = np.sum(self.nDofsArray)

        X0_e = self.Coord[:, self.IEN[:, e]]

        lM = np.zeros([nDofsElement, nDofsElement])

        for igp in range(ngp):
            # map from reference domain (r) -> (X), the derivatives at the gaussian point
            Jdet = np.fabs(np.dot(X0_e[0,:], sh1[igp, :, 1]))
            sh4_x = sh4[igp, :, 1]/Jdet
            # Local K and L
            mul = gpWeights1D[igp] * Jdet
            llM = np.block([[A * np.outer(sh1[igp, : , 0], sh1[igp, : , 0]), np.zeros((2,4))],
                            [np.zeros((4,2)), A * np.outer(sh4[igp, : , 0], sh4[igp, : , 0]) + I * np.outer(sh4_x, sh4_x)]]) * mul


            lM[0:n1+n2, 0:n1+n2] += rho * llM
        #todo mass matrix for the velocity
        lM[n1+n2:, n1+n2:] = np.identity(n1+n2)

        return lM
    def mass_matrix(self):
        '''
        Sparse matrix version csr_matrix
        M\dot{U} + L(U) - F = 0
        dL/d(U) = K
        :return: K, L, F
        '''
        nElements = self.nElements
        nEquations = self.nEquations
        LM = self.LM

        M = np.zeros((nEquations,nEquations))

        for e in range(nElements):

            lM = self.local_mass(e)

            # Step 3b: Get Global equation numbers
            PI = LM[:, e]

            # Step 3c: Eliminate Essential DOFs
            I = (PI >= 0)
            PI = PI[I]

            M[np.ix_(PI, PI)] += lM[np.ix_(I, I)]

        self.M = M

    def get_mass(self):
        return self.M

    def local_assembly(self, e, U_e, X0_e):
        '''
        assemble the RHS and its derivative
        :param U_e: 1d array, velocity and deformation
        :param X0_e: 2 by nNodesElement, undeformed coordinate
        :return:
        '''


        E, A, I = self.E, self.A, self.I

        sh1, sh4 = self.sh1, self.sh4

        n1, n2 = 2, 4

        ngp = self.ngp
        gpWeights1D = self.gpWeights1D


        nDofsElement = np.sum(self.nDofsArray)
        lK, lL = np.zeros([nDofsElement, nDofsElement]), np.zeros(nDofsElement)

        #displacement
        u_e, w_e, = U_e[n1+n2: 2*n1+n2], U_e[2*n1+n2: ]

        for igp in range(ngp):
            # map from reference domain (r,s) -> (X,Y), the derivatives at the gaussian point
            Jdet = np.fabs(np.dot(X0_e[0,:], sh1[igp, :, 1]))
            sh1_x = sh1[igp, :, 1]/Jdet
            sh4_x, sh4_xx = sh4[igp, :, 1]/Jdet, sh4[igp, :, 2]/Jdet/Jdet

            w_x = np.dot(w_e, sh4_x)
            N = E*A*(np.dot(u_e, sh1_x) + 0.5 * w_x**2)
            M = E*I*(-np.dot(w_e, sh4_xx))
            # Local K and L
            mul = gpWeights1D[igp] * Jdet
            # velocity update
            lL[0:n1] -= mul * N * sh1_x
            lL[n1:n1+n2] -= (N * w_x * sh4_x - M * sh4_xx)*mul

            lK[0:n1, n1+n2:2*n1+n2] -= mul * E * A * np.outer(sh1_x, sh1_x)
            K_aw = mul * E * A * w_x * np.outer(sh1_x, sh4_x)
            lK[0:n1, 2*n1+n2:2*(n1+n2)] -= K_aw
            lK[n1:n1+n2, n1+n2:2*n1+n2] -= K_aw.T
            lK[n1:n1+n2, 2*n1+n2:2*(n1+n2)] -= mul * E * I * np.outer(sh4_xx,sh4_xx) + mul*(E*A*w_x*w_x + N)*np.outer(sh4_x,sh4_x)


        # todo displacement update
        lL[n1+n2:] = U_e[0:n1+n2]
        lK[n1+n2:, 0:n1+n2] = np.identity(n1+n2)
        return lK, lL


    def assembly(self, U, C):
        '''
        M\dot{U} = L(U) + F(U)
        dL/d(U) = K
        :param U: state variables, 1d array
        :param C: coupling term, nodal force on the Neumann nodes
        :return: K, L
        '''

        nElements = self.nElements
        nEquations = self.nEquations
        LM,ID = self.LM, self.ID
        L = np.zeros((nEquations))
        K = np.zeros((nEquations, nEquations))

        U_all = self.get_full_field(U)

        for e in range(nElements):
            Ue, X0e = self.get_local_state(e, U_all, C)

            lK, lL = self.local_assembly(e, Ue, X0e)

            # Step 3b: Get Global equation numbers
            PI = LM[:, e]

            # Step 3c: Eliminate Essential DOFs
            I = (PI >= 0)
            PI = PI[I]

            # Step 3d: Assemble L

            L[PI] += lL[I]

            # Step 3e: Assemble K
            K[np.ix_(PI, PI)] += lK[np.ix_(I, I)]

        PI_r = ID[:, 0:self.nDof//2].flatten('F')
        PI_c = ID[:, :].flatten('F')
        I_r, I_c = (PI_r >= 0), (PI_c >= 0)
        PI_r, PI_c = PI_r[I_r], PI_c[I_c]

        load = C
        if load is not None:
            F = np.dot(self.matcher_trans_matrix.T, load.flatten('F'))
            L[PI_r] += F[I_r]




        return K, L






    def get_Qb(self, U):
        '''

        :param U: velocity and displacement , nEquations array
        :return: NeumannNodes, velocity and displacement, 1d array
        '''
        U_all = self.get_full_field(U)

        Qb = np.dot(self.matcher_trans_matrix, U_all.reshape((-1,2), order='F'))

        return Qb.flatten('F')


    def visualize(self, U, show_or_not = True):
        '''
        :param U:  fluid u,v,p array
        :param C:  mesh velocity and mesh displacement
        :return: plot the deformed shape
        '''

        U_all = self.get_full_field(U)

        X, Y = self.Coord[0, :], self.Coord[1, :]

        plt.scatter(X, Y, s=2, c='r', label='Initial')
        plt.scatter(X + U_all[:, 3], Y + U_all[:, 4], s=2, c='b', label='Final')

        if show_or_not:
            plt.show()

    def Newmark(self, t_n, dt, U_n, a_n, C, alpham, alphaf):
        '''
        Generalized alpha method for solving M\dotdot{u} = L(u) + F
        a, v, u are acceleration, velocity and displacement

        u_{n+1} = u_n + dtv_n + dt^2/2 ((1 - 2\beta)a_n + 2\beta a_{n+1})
        v_{n+1} = v_n + dt((1 - gamma)a_n + gamma a_{n+1})

        beta = (1 - alpham + alphaf)**2/4.0
        gamma = 1./2. - alpham + alphaf


        Forster, Christiane, Wolfgang A. Wall, and Ekkehard Ramm. "Artificial added mass instabilities in sequential staggered coupling of nonlinear structures and incompressible viscous flows."
        Computer methods in applied mechanics and engineering 196.7 (2007): 1278-1293.



        '''
        # active nodes number :nNodes
        n_disp = self.nEquations//2

        v_n, u_n = U_n[0: n_disp], U_n[n_disp: 2*n_disp]

        beta, gamma = (1 - alpham + alphaf)**2/4.0, 1./2. - alpham + alphaf

        M = self.get_mass()[0:n_disp, 0:n_disp]


        # Newton solve for a_{n+1}
        a_np = copy.copy(a_n)
        NewtonIterstep, NewtonConverge = 0, False
        eps, MaxIterstep = 1.0e-8, 100
        while not NewtonConverge:

            # validate gradient
            # app.check_derivative(u_n)

            NewtonIterstep = NewtonIterstep + 1
            u_np = u_n + dt * v_n + dt * dt / 2.0 * ((1 - 2 * beta) * a_n + 2 * beta * a_np)
            v_np = v_n + dt * ((1 - gamma) * a_n + gamma * a_np)


            a_nh = (1 - alpham)*a_np + alpham*a_n
            v_nh = (1 - alphaf)*v_np + alphaf*v_n
            u_nh = (1 - alphaf)*u_np + alphaf*u_n


            K_nh, L_nh = self.assembly(np.concatenate((v_nh, u_nh)), C)

            K_nh = K_nh[0:n_disp, n_disp:2*n_disp]

            res = M.dot(a_nh)  - L_nh[0:n_disp]

            A = (1 - alpham) * M - (1 - alphaf) * dt * dt * beta * K_nh

            delta_a_np = np.linalg.solve(A, res)

            a_np -= delta_a_np

            print("||F|| is ", np.linalg.norm(res))
            if (np.linalg.norm(res) < eps or NewtonIterstep > MaxIterstep):
                if NewtonIterstep > MaxIterstep:
                    print('Newton iteration cannot converge')
                NewtonConverge = True

        v_np = v_n + dt * ((1 - gamma) * a_n + gamma * a_np)
        u_np = u_n + dt * v_n + dt * dt / 2.0 * ((1 - 2 * beta) * a_n + 2 * beta * a_np)

        return np.concatenate((v_np,u_np)), a_np

if __name__ == '__main__':
    rho = 500.
    E = 250.
    A = 0.002

    nu = 0.0
    Nx, Ny = 2, 1
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen('Modified_Cavity_Beam', Nx, Ny)
    app1 = Beam(Nx, IEN, EBC, g, NBC, ID, Coord, problemType,elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations, rho, E, A)
    np.random.seed(101)
    Ue = np.random.normal(2.0, 0.2, np.sum(nDofsArray))
    X0_e = np.zeros((2,2))
    X0_e[0,:] = np.linspace(0,1./Nx, 2)


    unit = np.random.normal(1.0, 0.1, np.sum(nDofsArray))

    eps = 1.e-4
    Ue_p = Ue + eps*unit
    Ue_m = Ue - eps*unit



    lK,   lL   = app1.local_assembly(0, Ue, X0_e)
    lK_p, lL_p = app1.local_assembly(0, Ue_p, X0_e)
    lK_m, lL_m = app1.local_assembly(0, Ue_m, X0_e)


    error = (lL_p - lL_m)/(2*eps) - np.dot(lK,unit)

    print(error)

