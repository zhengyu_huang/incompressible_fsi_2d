import numpy as np
'''
The is for verification purpose
rho_f d U_f = K[0,0]*U_f + K[0,1]* U_s + K[0,2]*U_m  + A[0]*t     sys0
rho_s d U_s = K[1,0]*U_f + K[1,1]* U_s + K[1,2]*U_m  + A[1]*t     sys1
rho_m d U_m = K[2,0]*U_f +    K[2,1]* U_s + K[2,2]*U_m  + A[2]*t     sys2

K[2,0] = 0!!!!!!!!!

Cf = 0, U_s, U_m
Cs = U_f, 0, U_m
Cm = U_f, U_s, 0
'''

class Ode3:
    def __init__(self, rho, K, A, id = 0):
        '''
        This is a 1d ode
        rho*dx = r = aU[0] + b U[1] + c U[2] + d*t
        '''
        assert(abs(K[2,0]) < 1.e-10)
        self.id = id
        self.nEquations = 1
        self.t = 0.0

        # these nodes have velocity, displacement or Force updated at each time step
        self.rho = rho
        self.K = K
        self.A = A
        self.mass_matrix()

    def set_time(self, t):
        self.t = t
    def get_mass(self):
        return self.M
    def mass_matrix(self):
        '''
        M\dot{U} + L(U) - F = 0
        dL/d(U) = K
        :return: K, L, F
        '''

        M = np.zeros((self.nEquations, self.nEquations)) + self.rho

        self.M = M


    def assembly(self, U, CC):
        '''
        M\dot{U} = L(U) + F
        dL/d(U) = K
        :param U: state variables, 1d array
        :param C: coupling term
        :return: K, L
        '''

        t = self.t
        id = self.id

        nEquations = self.nEquations

        K = np.zeros((nEquations, nEquations))
        L = np.zeros((nEquations))

        K[:,:] += self.K[id, id]


        L[:]   += self.K[id, id]*U + self.K[id, 0]*CC[0] \
                  + self.K[id, 1]*CC[1] + self.K[id, 2]*CC[2] + t*self.A[id]

        return K, L

    def get_Qb(self, U1):
        '''
        :param U0:
        :param U1:
        :param U2:
        :return: C2
        '''

        C2 = [0, U1, 0]
        return C2

    def get_Qm(self, U2, C2):
        '''

        :param U2:
        :param C2:
        :return: C0
        '''
        U1 = C2[1]
        C0 = [0, U1, U2]
        return C0

    def get_force(self,U0, C0):
        '''

        :param U0:
        :param C0:
        :return: C1
        '''
        U2 = C0[2]
        C1 = [U0, 0, U2]
        return C1






