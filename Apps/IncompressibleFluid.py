import numpy as np
import matplotlib.pyplot as plt
from App import App
from MeshGen import  mesh_gen
class IncompressibleFluid(App):
    '''
    Donea, Jean, and Antonio Huerta. Finite element methods for flow problems. John Wiley & Sons, 2003. p. 286
    '''

    def __init__(self, Nx, Ny, IEN, EBC, g,  NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations, rho, nu):
        App.__init__(self, Nx, Ny, IEN, EBC, g,  NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations, rho)
        self.name = 'IncompressibleFluid'
        self.rho = rho
        self.nu = nu

    def set_wetted_surface(self, wettedNodes, wettedEdges):
        '''

        :param wettedNodes: [n0, n1 ... nk]
        :param wettedEdges:  k + 1 by 2, element-edge pair
        :return:
        : wettedMap: n0->0 ... nk->k
        : wettedIEN: 9 by nElement array, [i, e] = -1 or nj


        '''
        nNodesElement = self.nNodesElement
        Nx, Ny = self.Nx, self.Ny
        IEN = self.IEN

        self.wettedEdges = wettedEdges

        wettedMap = {}
        for i in range(len(wettedNodes)):
            wettedMap[wettedNodes[i]] = i #the order in the structure solver
        self.wettedMap = wettedMap

        wettedIEN = np.zeros((nNodesElement, Nx * Ny), dtype=int) - 1

        local_edge = [[0,4,1],[1,5,2],[2,6,3],[3,7,0]]
        for e, id in wettedEdges:
            for i in local_edge[id]:
                wettedIEN[i, e] = wettedMap[IEN[i, e]]
        self.wettedIEN = wettedIEN


    def get_full_field(self, U, C):
        '''
        :param U: fluid ug,vg,pg array, 1d array
        :param C: mesh velocity and mesh displacement, 1d array
        :return: U_all: velocity and pressure of all nodes, nNodes by 3 array
                 (pressure empty freedoms are garbage)
        '''

        nNodes, nElements = self.nNodes, self.nElements
        nDof = self.nDof
        EBC, g = self.EBC, self.g
        ID = self.ID
        nDofsArray = self.nDofsArray
        IEN, Coord = self.IEN, self.Coord

        U_all = np.zeros((nNodes, nDof))
        I = (EBC == 0)
        U_all[I] = U[ID[I]]


        # add essential boundary conditions

        for e in range(nElements):
            n = IEN[:, e]
            for j in range(nDof):  # freedom
                for i in range(nDofsArray[j]):  # node local id
                    if EBC[n[i],j] > 0:

                        if EBC[n[i], j] == 1:
                            #essential boundary condition
                            U_all[n[i], j] = g[n[i], j]
                        elif EBC[n[i], j] == 2:
                            # time variant boundary condition
                            U_all[n[i], j] = self.boundary_condition(n[i], j)
                        elif EBC[n[i],j] == 3:
                            # FSI velocity boundary condition
                            U_all[n[i], j] = C[j * nNodes + n[i]]

        return U_all

    def boundary_condition(self,  nId, dofId):
        t = self.t
        if self.problemType == 'Modified_Cavity_Fluid':
            if dofId == 0:
                return (1 - np.cos(2. * np.pi * t / 5.))
            elif dofId == 1:
                return 0.

    def get_local_state(self, e,  U_all, C):
        '''

        :param e: element id
        :param U_all: fluid ug, vg, pg array
        :param C: mesh velocity and mesh displacement
        :return: U_e: local fluid ug, vg, pg 1d array
                 C_e: local mesh velocity and mesh displacement 1d array
        '''
        IEN = self.IEN
        nNodes = self.nNodes
        nDofsArray = self.nDofsArray

        n = IEN[:, e]

        U_e = np.concatenate((U_all[n,0], U_all[n,1], U_all[n[0:nDofsArray[2]],2]))

        C_e = np.concatenate((C[n], C[n + nNodes], C[n + 2*nNodes], C[n + 3*nNodes]), axis = 0)

        return U_e, C_e

    def local_mass(self, e):
        '''
        :param    e: element number
        :return: lM: local mass matrix
        '''
        sh2 = self.sh2
        n1 = self.nDofsArray[0]

        ngpt = self.ngpt
        gpWeights2D = self.gpWeights2D

        nDofsElement = np.sum(self.nDofsArray)

        X0_e = self.Coord[:, self.IEN[:, e]]

        lM = np.zeros([nDofsElement, nDofsElement])

        for igp in range(ngpt):
            # map from reference domain (r,s) -> (X,Y), the derivatives at the gaussian point
            J = np.dot(X0_e, sh2[igp, :, 1:3])
            Jdet = J[0, 0] * J[1, 1] - J[0, 1] * J[1, 0]

            mul = gpWeights2D[igp] * Jdet

            llM = np.outer(sh2[igp, :, 0], mul * sh2[igp, :, 0])

            lM[0:n1, 0:n1] += llM
            lM[n1:2 * n1, n1:2 * n1] += llM


        return lM

    def local_assembly(self, e, f_e, m_e):
        '''
        Compute the right hand side with Weakly enfored boundary conditions
        :param e: element id
        :param f_e: u*g, v*g, p*g fluid 1d array
        :param m_e: mesh velocity and mesh displacement 1d array
        :return: lL and lK
        '''
        assert (self.elementType == 'Q2Q1')

        n1, n2 = self.nDofsArray[0], self.nDofsArray[2]
        sh1, sh2 = self.sh1, self.sh2
        ngp, ngpt = self.ngp, self.ngpt
        gpWeights2D = self.gpWeights2D

        lK, lL = np.zeros([2 * n1 + n2, 2 * n1 + n2]), np.zeros(2 * n1 + n2)

        # velocity and pressure of each freedom
        u_e, v_e, p_e = f_e[0:n1], f_e[n1:2*n1], f_e[2 * n1:2 * n1 + n2]
        # velocity of mesh
        um_e, vm_e  = m_e[0:n1], m_e[n1:2 * n1]

        # mesh position of each freedom
        X0_e = self.Coord[:, self.IEN[:, e]]
        X_e = X0_e + np.reshape(m_e[2 * n1:4 * n1], (2, n1))

        for igp in range(ngpt):
            # map from reference domain (r,s) -> (x,y), the derivatives at the gaussian point
            J = np.dot(X_e, sh2[igp, :, 1:3])
            Jdet = J[0, 0] * J[1, 1] - J[0, 1] * J[1, 0]
            Jinv = np.array([[J[1, 1] / Jdet, -J[0, 1] / Jdet], [-J[1, 0] / Jdet, J[0, 0] / Jdet]])

            # x, y derivatives of shape functions
            sh2_xy = np.dot(sh2[igp, :, 1:3], Jinv)

            # Solution and derivatives, here u and v are relative velocity
            u, v, p = np.dot(u_e - um_e, sh2[igp, :, 0]), np.dot(v_e - vm_e, sh2[igp, :, 0]), np.dot(p_e, sh1[igp, :, 0])

            u_x, u_y = np.dot(u_e, sh2_xy[:, :])
            v_x, v_y = np.dot(v_e, sh2_xy[:, :])


            # Local K and L
            mul = gpWeights2D[igp] * Jdet

            # part of the inviscid flux
            lL[0:n1] -= mul * (u_x * u + u_y * v) * sh2[igp, :, 0]
            lL[n1:2 * n1] -= mul * (v_x * u + v_y * v) * sh2[igp, :, 0]

            # Pressure
            lL[0:n1] += mul * p/self.rho * sh2_xy[:, 0]
            lL[n1:2 * n1] += mul * p/self.rho * sh2_xy[:, 1]

            # viscous term
            lL[0:n1] -= mul * 2*self.nu*(u_x * sh2_xy[:, 0]  + (u_y + v_x)/2.0 * sh2_xy[:, 1])
            lL[n1:2 * n1] -= mul * 2*self.nu*((u_y + v_x)/2.0 * sh2_xy[:, 0] + v_y * sh2_xy[:, 1])

            # divergence free
            lL[2 * n1:2 * n1 + n2] += mul * (u_x + v_y) * sh1[igp, :, 0]

            # part of the invisicid flux
            lK[0:n1, 0:n1] -= np.outer(sh2[igp, :, 0],
                                       (mul * u_x * sh2[igp, :, 0] + mul * u * sh2_xy[:, 0] + mul * v * sh2_xy[:, 1]))
            lK[0:n1, n1:2 * n1] -= np.outer(sh2[igp, :, 0], (mul * u_y * sh2[igp, :, 0]))
            lK[n1:2 * n1, 0:n1] -= np.outer(sh2[igp, :, 0], (mul * v_x * sh2[igp, :, 0]))
            lK[n1:2 * n1, n1:2 * n1] -= np.outer(sh2[igp, :, 0],
                                       (mul * v_y * sh2[igp, :, 0] + mul * v * sh2_xy[:, 1] + mul * u * sh2_xy[:, 0]))

            #pressure
            lK[0:n1, 2 * n1:2 * n1 + n2] += np.outer(sh2_xy[:, 0], (mul/self.rho * sh1[igp, :, 0]))
            lK[n1:2 * n1, 2 * n1:2 * n1 + n2] += np.outer(sh2_xy[:, 1], (mul/self.rho * sh1[igp, :, 0]))

            #viscous term
            lK[0:n1, 0:n1] -= np.outer(mul * 2 * self.nu * sh2_xy[:, 0], sh2_xy[:, 0]) \
                              + np.outer(mul * self.nu * sh2_xy[:, 1], sh2_xy[:, 1])
            lK[0:n1, n1:2*n1] -= np.outer(mul * self.nu * sh2_xy[:, 1], sh2_xy[:, 0])
            lK[n1:2 * n1, n1:2 * n1] -= np.outer(mul * 2 * self.nu * sh2_xy[:, 1], sh2_xy[:, 1]) \
                                        + np.outer(mul * self.nu * sh2_xy[:, 0], sh2_xy[:, 0])
            lK[n1:2 * n1, 0:n1] -= np.outer(mul * self.nu * sh2_xy[:, 0], sh2_xy[:, 1])

            # divergence free
            lK[2 * n1:2 * n1 + n2, 0:n1] += np.outer(mul * sh1[igp, :, 0], sh2_xy[:, 0])
            lK[2 * n1:2 * n1 + n2, n1:2 * n1] += np.outer(mul * sh1[igp, :, 0], sh2_xy[:, 1])

        return lK, lL



    def local_free_bc_assembly(self, e, f_e, m_e, id):
        '''
        Compute the right hand side with Weakly enfored boundary conditions
        :param e: element id
        :param f_e: u*g, v*g, p*g fluid 1d array
        :param m_e: mesh velocity and mesh displacement 1d array
        :return: lL and lK
        '''
        assert (self.elementType == 'Q2Q1')

        n1, n2 = self.nDofsArray[0], self.nDofsArray[2]
        nNodesEdge = 3
        sh1, sh2 = self.sh1, self.sh2
        ngp, ngpt = self.ngp, self.ngpt
        
        gpWeights1D = self.gpWeights1D
        she1, she2 = self.she1, self.she2
        
        NN = np.array([[0., -1.], [1., 0.], [0., 1.], [-1., 0.]])
        edgeGP = np.array([[0, 1, 4], [1 ,2, 5], [2, 3, 6], [3, 0, 7]], dtype=int)


        lK, lL = np.zeros([2 * n1 + n2, 2 * n1 + n2]), np.zeros(2 * n1 + n2)

        # velocity and pressure of each freedom
        U_e,  p_e = np.reshape(f_e[0:2 * n1], (2, n1)), f_e[2 * n1:2 * n1 + n2]
        
        # velocity of mesh
        um_e, vm_e  = m_e[0:n1], m_e[n1:2 * n1]

        # mesh position of each freedom
        X0_e = self.Coord[:, self.IEN[:, e]]
        X_e = X0_e + np.reshape(m_e[2 * n1:4 * n1], (2, n1))

        N = NN[id, :] #normal vector
        lF = np.zeros(2)
        dlF = np.zeros((2, 2 * n1 + n2))
        
        for igp in range(ngp):
            # map from reference domain (r,s) -> (x,y), the derivatives at the gaussian point
            J = np.dot(X0_e, she2[id, igp, :, 1:3])  # d(X,Y)/d(r,s)
            Jdet = J[0, 0] * J[1, 1] - J[0, 1] * J[1, 0]
            Jinv = np.array([[J[1, 1] / Jdet, -J[0, 1] / Jdet], [-J[1, 0] / Jdet, J[0, 0] / Jdet]])

            # map from reference domain (r,s) -> (x, y), the derivatives at the gaussian point
            G0 = np.dot(X_e, she2[id, igp, :, 1:3])  # d(x,y)/d(r,s)
            G0det = G0[0, 0] * G0[1, 1] - G0[0, 1] * G0[1, 0]
            G0inv = np.array([[G0[1, 1] / G0det, -G0[0, 1] / G0det], [-G0[1, 0] / G0det, G0[0, 0] / G0det]])
            
            # map from reference domain (X,Y) -> (x,y), the derivatives at the gaussian point
            G = np.dot(G0, Jinv)  # d(x,y)/d(X,Y)
            g = G[0, 0] * G[1, 1] - G[0, 1] * G[1, 0]
            Ginv = np.array([[G[1, 1] / g, -G[0, 1] / g], [-G[1, 0] / g, G[0, 0] / g]])

            she2_xy = np.dot(she2[id, igp, :, 1:3], G0inv)


            U, p = np.dot(U_e, she2[id, igp, :, 0]), np.dot(p_e, she1[id, igp, :, 0])

            U_xy = np.dot(U_e, she2_xy)


            mul = gpWeights1D[igp] * (np.linalg.norm(G0[:,0]) if id%2==0 else np.linalg.norm(G0[:,1]))
            
            n = np.dot(Ginv.T, N)
            lF[:] = np.dot(np.array([[p, 0],[0, p]]) - self.nu*(U_xy + U_xy.T), n) / self.rho * g * mul 

            dlF[:, 2 * n1:2 * n1 + n2] = np.outer(n, she1[id, igp, :, 0]) / self.rho * g * mul
            dlF[0, 0:n1]             = -self.nu*(2.0*she2_xy[:,0]*n[0] + she2_xy[:,1]*n[1]) / self.rho * g * mul
            dlF[0, n1:2*n1]          = -self.nu*(she2_xy[:,0]*n[1]) / self.rho * g * mul
            dlF[1, 0:n1]             = -self.nu*(she2_xy[:,1]*n[0]) / self.rho * g * mul
            dlF[1, n1:2*n1]          = -self.nu*(she2_xy[:,0]*n[0] + 2.0*she2_xy[:,1]*n[1]) / self.rho * g * mul
            
            for j in range(nNodesEdge):
                loc = edgeGP[id,j]
                lL[loc] -= lF[0] * she2[id, igp, loc, 0]
                lL[loc + n1] -= lF[1] * she2[id, igp, loc, 0]

                lK[loc, :]      -=  dlF[0, :] * she2[id, igp, loc, 0]
                lK[loc + n1, :] -=  dlF[1, :] * she2[id, igp, loc, 0]
             
        return lK, lL

    def get_force(self, U, C):
        '''

        :param U: fluid u,v,p array, 1d array
        :param C: mesh velocity and mesh displacement, 1d array
        :return: F: force loard on these wetted nodes, n by 2 array
        '''
        n1, n2 = 9, 4
        nNodesEdge = 3
        wettedMap = self.wettedMap
        wettedEdges = self.wettedEdges #[e, id]
        F = np.zeros([len(wettedMap), self.nDim])
        # outward normal in the undeformed domain
        NN = np.array([[0., -1.], [1., 0.], [0., 1.], [-1., 0.]])
        edgeGP = np.array([[0, 1, 4], [1 ,2, 5], [2, 3, 6], [3, 0, 7]], dtype=int)

        ngp = self.ngp

        #use quadrature points on the edges, for pressure and for velocity
        gpWeights1D = self.gpWeights1D
        she1, she2 = self.she1, self.she2

        U_all = self.get_full_field(U,C)
        for e, id in wettedEdges:
            # element id: e, local edge id: id
            f_e, m_e = self.get_local_state(e,  U_all, C)

            # velocity and pressure of each freedom
            U_e, p_e = np.reshape(f_e[0:2 * n1], (2, n1)), f_e[2 * n1:2 * n1 + n2]
            # mesh position of each freedom
            X0_e = self.Coord[:, self.IEN[:, e]]
            X_e = X0_e + np.reshape(m_e[2 * n1:4 * n1], (2, n1))
            N = NN[id, :] #normal vector



            for igp in range(ngp):
                # map from reference domain (r,s) -> (X, Y), the derivatives at the gaussian point
                J = np.dot(X0_e, she2[id, igp, :, 1:3])  # d(X,Y)/d(r,s)
                Jdet = J[0, 0] * J[1, 1] - J[0, 1] * J[1, 0]
                Jinv = np.array([[J[1, 1] / Jdet, -J[0, 1] / Jdet], [-J[1, 0] / Jdet, J[0, 0] / Jdet]])

                # map from reference domain (r,s) -> (x, y), the derivatives at the gaussian point
                G0 = np.dot(X_e, she2[id, igp, :, 1:3])  # d(x,y)/d(r,s)
                G0det = G0[0, 0] * G0[1, 1] - G0[0, 1] * G0[1, 0]
                G0inv = np.array([[G0[1, 1] / G0det, -G0[0, 1] / G0det], [-G0[1, 0] / G0det, G0[0, 0] / G0det]])


                # map from reference domain (X,Y) -> (x,y), the derivatives at the gaussian point
                G = np.dot(G0, Jinv)  # d(x,y)/d(X,Y)
                g = G[0, 0] * G[1, 1] - G[0, 1] * G[1, 0]
                Ginv = np.array([[G[1, 1] / g, -G[0, 1] / g], [-G[1, 0] / g, G[0, 0] / g]])



                she2_xy = np.dot(she2[id, igp, :, 1:3], G0inv)


                # pressure, p
                # shear stress, sigma
                # wall normal n

                U, p = np.dot(U_e, she2[id, igp, :, 0]), np.dot(p_e, she1[id, igp, :, 0])

                U_xy = np.dot(U_e, she2_xy)



                mul = gpWeights1D[igp] * (np.linalg.norm(G0[:,0]) if id%2==0 else np.linalg.norm(G0[:,1]))

                lF = np.dot(np.array([[p, 0],[0, p]]) - self.nu*self.rho*(U_xy + U_xy.T), np.dot(Ginv.T, N)) * g * mul


                for j in range(nNodesEdge):
                    loc = edgeGP[id,j]
                    F[wettedMap[self.IEN[loc,e]], :] += lF * she2[id, igp, loc, 0]
        return F

    def visualize(self, U, C, qname):
        '''

        :param e:
        :param U:  fluid u,v,p array
        :param C:  mesh velocity and mesh displacement
        :param dofId:
        :return:
        '''
        assert(self.elementType == 'Q2Q1')
        nNodes, Nx, Ny = self.nNodes, self.Nx, self.Ny
        U_all = self.get_full_field(U, C)
        X = np.reshape(self.Coord[0, :] + C[2 * nNodes: 3 * nNodes], (2*Nx+1, 2*Ny+1))
        Y = np.reshape(self.Coord[1, :] + C[3 * nNodes:4 * nNodes], (2*Nx+1, 2*Ny+1))
        from scipy.interpolate import griddata
        
        if qname == "pressure":
            plt.figure(figsize=(6, 6), dpi=80)

            Qoi = np.reshape(U_all[:,2], (2*Nx+1, 2*Ny+1))
            
            plt.pcolormesh(X[0::2, 0::2], Y[0::2, 0::2], Qoi[0::2, 0::2], shading='gouraud', vmin=-0.45, vmax=0.25, zorder=1)

            print("pressure is ", Qoi[0::2, 0::2].max(), Qoi[0::2, 0::2].min())
            
            ux, uy =  np.reshape(U_all[:,0], (2*Nx+1, 2*Ny+1)),  np.reshape(U_all[:,1], (2*Nx+1, 2*Ny+1))

            plt.axis('equal')
            plt.set_cmap('RdBu')

            x_g = np.linspace(X.min(), X.max(), 40)
            y_g = np.linspace(Y.min(), Y.max(), 40)



            xi, yi = np.meshgrid(x_g, y_g)

            # then, interpolate your data onto this grid:


            px = X.flatten()
            py = Y.flatten()

            points = np.zeros((len(px),2))
            points[:,0], points[:,1] = px, py
            print(points.shape)


            pu = ux.flatten()
            pv = uy.flatten()
            print(len(px), len(pu),len(py), len(pv))

            gu = griddata(points, pu, (xi, yi), method="cubic")
            gv = griddata(points, pv, (xi, yi), method="cubic")

            plt.streamplot(xi, yi, gu, gv,linewidth = 1, color="black", zorder=2)

            plt.fill_between(X[0,:], np.zeros(2*Nx+1), Y[0,:], facecolor='white', zorder=3)

            plt.xlim([0, 1])
            plt.ylim([0, 1])

            plt.axis('off')


        if qname == "velocity":
            ux, uy =  np.reshape(U_all[:,0], (2*Nx+1, 2*Ny+1)),  np.reshape(U_all[:,1], (2*Nx+1, 2*Ny+1))
            Qoi = np.sqrt(ux**2 + uy**2)

            plt.figure(figsize=(6, 6), dpi=80)
            plt.pcolormesh(X, Y, Qoi, shading='gouraud', vmin=0.0, vmax=2.0)

            plt.axis('equal')
            plt.set_cmap('cool')

            x_g = np.linspace(X.min(), X.max(), 40)
            y_g = np.linspace(Y.min(), Y.max(), 40)



            xi, yi = np.meshgrid(x_g, y_g)

            # then, interpolate your data onto this grid:


            px = X.flatten()
            py = Y.flatten()

            points = np.zeros((len(px),2))
            points[:,0], points[:,1] = px, py
            print(points.shape)


            pu = ux.flatten()
            pv = uy.flatten()
            print(len(px), len(pu),len(py), len(pv))

            gu = griddata(points, pu, (xi, yi), method="cubic")
            gv = griddata(points, pv, (xi, yi), method="cubic")

            plt.streamplot(xi, yi, gu, gv,linewidth = 1, color="white")

            #plt.fill_between(X[0,:], np.zeros(2*Nx+1), Y[0,:], facecolor='white')

            plt.xlim([0, 1])
            plt.ylim([0, 1])

            plt.axis('off')


            #plt.show()






if __name__ == '__main__':
    np.random.seed(11)
    test_id = 0
    rho = 1.
    nu = 0.01
    Nx, Ny = 10, 10
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen(
        'Modified_Cavity_Fluid', Nx,
        Ny)
    ngp = 3
    app0 = IncompressibleFluid(Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray,
                               nElements, nNodes, nEquations, rho, nu)
    wettedNodesF = np.arange(0, 2 * Nx + 1, dtype=int)
    wettedEdgesF = np.zeros((Nx, 2), dtype=int)
    wettedEdgesF[:, 0] = np.arange(Nx)
    app0.set_wetted_surface(wettedNodesF, wettedEdgesF)


    if test_id == 0:
        Uf_e = np.random.normal(2.0, 0.2, np.sum(nDofsArray))
        Um_e = np.random.normal(2.0, 0.2, 4 * 9)


        unit = np.random.normal(1.0, 0.1, np.sum(nDofsArray))
        # unit[9:] = 0.0
        eps = 1.e-3
        Ufe_p = Uf_e + eps * unit
        Ufe_m = Uf_e - eps * unit

        e = Nx * (Ny - 1)
        e = Nx * (Ny - 2)
        e = Nx * Ny - 1
        e = Nx * (Ny - 1) - 1

        lK, lL = app0.local_assembly(e, Uf_e, Um_e)
        lK_p, lL_p = app0.local_assembly(e, Ufe_p, Um_e)
        lK_m, lL_m = app0.local_assembly(e, Ufe_m, Um_e)

        error = (lL_p - lL_m) / (2 * eps) - np.dot(lK, unit)
        print(error)

    if test_id == 1:
        Uf = np.random.normal(2.0, 0.2, app0.nEquations)
        C = np.random.normal(2.0, 0.2, app0.nNodes * 4)

        C = np.zeros(app0.nNodes * 4)

        unit = np.random.normal(1.0, 0.1, 4*(2 * app0.Nx + 1))
        unit_expand = np.zeros(4*app0.nNodes)

        unit_expand[app0.nNodes * 0: app0.nNodes * 0 + Nx * 2 + 1] = unit[0:2 * Nx + 1]
        unit_expand[app0.nNodes * 1: app0.nNodes * 1 + Nx * 2 + 1] = unit[2 * Nx + 1:   2*(2 * Nx + 1)]
        unit_expand[app0.nNodes * 2: app0.nNodes * 2 + Nx * 2 + 1] = unit[2*(2*Nx + 1): 3*(2*Nx + 1)]
        unit_expand[app0.nNodes * 3: app0.nNodes * 3 + Nx * 2 + 1] = unit[3*(2*Nx + 1): 4*(2*Nx + 1)]


        unit_c = np.zeros(18)
        id = np.array([0, 2, 4*Nx+4, 4*Nx + 2, 1, 2*Nx+3, 4*Nx+3, 2*Nx+1, 2*Nx+2])
        unit_c[0:9], unit_c[9:18] = unit_expand[id + 2*app0.nNodes], unit_expand[id + 3*app0.nNodes]


        print('unit_c ', unit_c)

        # unit[9:] = 0.0
        eps = 1.e-4
        C_p = C + eps * unit_expand
        C_m = C - eps * unit_expand

        F, dF, dF_dC, lF, dlF = app0.get_force(Uf, C, True)

        F_p, _, _, lF_p, _ = app0.get_force(Uf, C_p, True)

        F_m, _, _, lF_m, _ = app0.get_force(Uf, C_m, True)

        error = (F_p.flatten(order='F') - F_m.flatten(order='F')) / (2 * eps) - np.dot(dF, unit)
        error_expand = (F_p.flatten(order='F') - F_m.flatten(order='F')) / (2 * eps) - np.dot(dF_dC, unit_expand)

        error_c = (lF_p - lF_m) / (2 * eps) - np.dot(dlF, unit_c)



        print('err ', error,'\n err_exp ', error_expand, ' \n error_c ', error_c)

