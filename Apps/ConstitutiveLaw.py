import numpy as np
def constitutive_law(EE, type, arg):
    '''

    :param EE: strain, 3 by 1, [eps_11, eps_22, 2 eps_12]
    :param type: material type
    :param arg: Young's modulus and Poisson's ratio
    :return:
            S : 3 by 1
            dS_dE: 3 by 3
    '''
    if type == 'PlaneStress':
        E, v = arg
        De = E / (1 - v ** 2) * np.array([[1, v, 0], [v, 1, 0], [0, 0, (1 - v) / 2]])

        S = np.dot(De, EE)
        return S, De



    elif type == 'PlaneStrain':
        E, v = arg
        De = E / ((1 + v) * (1 - 2 * v)) * np.array([[1 - v, v, 0], [v, 1 - v, 0], [0, 0, (1 - 2 * v) / 2]])

        S = np.dot(De, EE)
        return S, De