import numpy as np
from scipy.sparse.linalg import spsolve
from scipy.sparse import isspmatrix


def Linear_Solver(app, U, func, args):
    '''
    Solve for F(U) = 0
    :param U0:
    :param F:
    :param dF:
    :return:
    '''

    sys_id = args[-1]

    F, dF = func(app, U, *args)
    dU = -spsolve(dF, F)
    res = np.linalg.norm(F)
    print('||F|| = ', res)
    U[sys_id] += dU

    return U

def Newton_Solver(app, U, func, args, alpha = 1.0):
    '''
    Solve for F(U) = 0
    :param U0:
    :param F:
    :param dF:
    :return:
    '''

    MAXITE = 100
    eps = 1.e-8
    res0 = np.inf
    sys_id = args[-1]
    for ite in range(MAXITE):
        F, dF = func(app, U, *args)

        dU = -spsolve(dF, F) if isspmatrix(dF) else -np.linalg.solve(dF, F)
        res = np.linalg.norm(F)

        while alpha * res > res0:
            alpha /= 2.0
            print('reduce alpha', alpha)
        U[sys_id] += alpha*dU


        print('Ite = ', ite, '||F|| = ', res)
        if (res < eps):
            break
        alpha = min(1., 2*alpha)
        res0 = res
    return U






class FSI:
    '''
    This is fluid-structure-mesh 3way coupled partitioned solver
    '''
    def __init__(self, app0, app1, app2, pref = 'bin/'):
        self.app0 = app0 #fluid
        self.app1 = app1 #structure
        self.app2 = app2 #mesh

        self.app = [app0, app1, app2]

        self.N0 = app0.nEquations
        self.N1 = app1.nEquations
        self.N2 = app2.nEquations

        self.pref =  pref


    def reset_N(self, N0, N1, N2):
        self.N0 = N0
        self.N1 = N1
        self.N2 = N2

    def set_time(self, t):
        self.app0.set_time(t)
        self.app1.set_time(t)
        self.app2.set_time(t)


    def divide(self, U):
        N0, N1, N2 = self.N0, self.N1, self.N2
        U0,U1,U2 = U[0:N0], U[N0:N0+N1], U[N0+N1:]
        return U0, U1, U2

    def combine(self, U0, U1, U2):
        U = np.concatenate((U0, U1, U2))
        return U

    def get_coupling_0(self, U0, U1, U2):
        # mesh velocity and displacement
        C2 = self.get_coupling_2(U0, U1, U2)
        C0 = self.app2.get_Qm(U2, C2)
        return C0

    def get_coupling_1(self, U0, U1, U2):
        # force load on the wetted surface
        C0 = self.get_coupling_0(U0, U1, U2)
        C1 = self.app0.get_force(U0, C0)
        return C1

    def get_coupling_2(self, U0, U1, U2):
        # wetted surface velocity and displacement
        C2 = self.app1.get_Qb(U1)
        return C2

    def get_coupling_term(self, U, U_lag, sys_id):
        if sys_id == 1:
            # force load on the wetted surface

            C2 = self.app1.get_Qb(U_lag[1])
            C0 = self.app2.get_Qm(U_lag[2], C2)
            C1 = self.app0.get_force(U_lag[0], C0)

            return C1

        elif sys_id == 2:

            # wetted surface velocity and displacement
            C2 = self.app1.get_Qb(U[1])
            return C2

        elif sys_id == 0:

            # mesh velocity and displacement
            C2 = self.app1.get_Qb(U[1])
            C0 = self.app2.get_Qm(U[2], C2)
            return C0

        elif sys_id == -1:

            # force load on the wetted surface
            # wetted surface velocity and displacement
            # mesh velocity and displacement
            C2 = self.app1.get_Qb(U_lag[1])
            C0 = self.app2.get_Qm(U_lag[2], C2)
            C1 = self.app0.get_force(U_lag[0], C0)
            return (C0, C1, C2)

    def velo_cpl(self, U, C):
        # get right hand sides of the equation, using state U and coupling term C
        U0, U1, U2 = self.divide(U)
        C0, C1, C2 = C

        _, L0 = self.app0.assembly(U0, C0)
        _, L1 = self.app1.assembly(U1, C1)
        _, L2 = self.app2.assembly(U2, C2)

        return self.combine(L0, L1, L2)

    def velo(self, U):
        # get right hand sides of the equation, using state U only
        U0, U1, U2 = self.divide(U)
        C0, C1, C2 = self.get_coupling_0(U0, U1, U2), self.get_coupling_1(U0, U1, U2), self.get_coupling_2(U0, U1, U2)

        _, L0 = self.app0.assembly(U0, C0)
        _, L1 = self.app1.assembly(U1, C1)
        _, L2 = self.app2.assembly(U2, C2)

        return self.combine(L0, L1, L2)

    def mass_rmult(self, U):
        U0, U1, U2 = self.divide(U)
        MU0 = self.app0.get_mass().dot(U0)
        MU1 = self.app1.get_mass().dot(U1)
        MU2 = self.app2.get_mass().dot(U2)
        return self.combine(MU0, MU1, MU2)


    def mass_inv_rmult(self, U):
        U0, U1, U2 = self.divide(U)
        MinvU0 = spsolve(self.app0.get_mass(), U0)
        MinvU1 = np.linalg.solve(self.app1.get_mass(), U1)
        MinvU2 = spsolve(self.app2.get_mass(), U2)
        return self.combine(MinvU0, MinvU1, MinvU2)


    def save_data(self, U, id):
        pref = self.pref
        np.save(pref + str(id).zfill(5) + '.dat', U)


    def visualize_data(self, U):

        U0, U1, U2 = self.divide(U)
        print(len(U0), len(U1), len(U2))

        C0 = self.get_coupling_0(U0, U1, U2)

        C2 = self.get_coupling_2(U0, U1, U2)

        self.app0.visualize(U0, C0, "velocity")
        self.app0.visualize(U0, C0, "pressure")

        self.app1.visualize(U1) #structure visulization, it does not dependent on C1

        self.app2.visualize(U2, C2)


    def get_force(self, U):
        # get force load on the fluid-structure interface
        U0, U1, U2 = self.divide(U)

        C1 = self.get_coupling_1(U0, U1, U2)

        return C1



