This is an educational code for solving fluid structure interaction (FSI) problems with a partitioned spectral deferred correction solver[1].


This code requires Python 3, there are detailed implementation documents in doc/Incompressible_FSI_2D.html or doc/Incompressible_FSI_2D.md.


Examples folder contains
Cavity.py: cavity flow problem

Modified_Cavity_Beam_SDC.py: cavity flow with flexible bottom, a three way coupled FSI problem, which is solved by partitioned spectral deferred correction (SDC) solver

Modified_Cavity_Beam_SSA.py: cavity flow with flexible bottom, a three way coupled FSI problem, which is solved by a sequential staggered solver

BeamBending.py: clapped beam problem

Ode_System.py: ode system, a three way coupled model problem, which is solved by partitioned spectral deferred correction (SDC) solver



Reference:

[1] Daniel Z. Huang, Will Pazner, Per-olof Persson, and Matthew J. Zahr. "High-order partitioned spectral deferred correction solvers for muliphysics problems"

[2] Förster, Christiane, Wolfgang A. Wall, and Ekkehard Ramm. "Artificial added mass instabilities in sequential staggered coupling of nonlinear structures and incompressible viscous flows."
Computer methods in applied mechanics and engineering 196.7 (2007): 1278-1293.

[3] Donea, Jean, and Antonio Huerta. Finite element methods for flow problems. John Wiley & Sons, 2003.

[4] De Borst, René, et al. Nonlinear finite element analysis of solids and structures. John Wiley & Sons, 2012. p 308
