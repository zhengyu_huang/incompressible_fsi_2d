import sys
sys.path.insert(0, '../Apps')
import numpy as np
import copy

from FSI import Newton_Solver, Linear_Solver

def sdc_method_coefficients(sdc_type):
    print("sdc type is ", sdc_type)
    if sdc_type == 'radau1':
        ts = np.array([0., 1.])
        q = np.array([0., 1.])
        order = 1

    elif sdc_type == 'lobatto2':
        ts = np.array([0., 1.])
        q = np.array([0.5, 0.5])
        order = 2

    elif sdc_type == 'radau3':
        ts = np.array( [0., 0.5, 1.])
        q = np.array([5./24., 8./24., -1./24.,
                      -1./24., 8./24., 5./24.])

#        ts = np.array([0., 1./3., 1.])
#        q = np.array([0., 5./12., -1./12.,
#                     0., 4./12., 4./12.])
        order = 3
    elif sdc_type == 'radau3-r':

        ts = np.array([0., 1./3., 1.])
        q = np.array([0., 5./12., -1./12.,
                      0., 4./12., 4./12.])
        order = 3

    elif sdc_type == 'lobatto4':
        ts = np.array( [0., 0.5, 1.])
        q = np.array([5./24., 8./24., -1./24.,
                      -1./24., 8./24., 5./24.])
        order = 4

    elif sdc_type == 'radau5':
        ts = np.array([0., 1./10.*(4.-np.sqrt(6.)), 1./10.*(4.+np.sqrt(6.)), 1.0])
        q = np.array([0., 1./360.*(88.-7.*np.sqrt(6.)), (296.-169.*np.sqrt(6.))/1800., 1./225.*(-2.+3.*np.sqrt(6.)),
                      0., 1./150.*(-12. + 17.*np.sqrt(6.)), 1./150.*(12. + 17.*np.sqrt(6.)), -((2.*np.sqrt(2./3.))/25.),
                      0., 1./600.*(168. - 73.*np.sqrt(6.)), 1./120.*(24. + np.sqrt(6.)), 1./75.*(9. + np.sqrt(6.))])
        order = 5

    nPoints = len(ts)
    niters = order
    return q, ts, nPoints, niters

def sdc_residual(app, U, U_lag, R, dt, sys_id):
    '''

    :param R:
    :param sys_id:
    :return:
    res = M u - dt *r(u, c(u, ...)) + R
    dres_du
    '''

    C = app.get_coupling_term(U, U_lag, sys_id)
    K, L = app.app[sys_id].assembly(U[sys_id], C)
    M = app.app[sys_id].get_mass()
    res = M.dot(U[sys_id]) - dt * L + R
    dres = M - dt *K

    return res, dres

def sdc_solve(app, Um, U_lag, R, dt):
    '''

    :param Um: Initial guess
    :param U_lag: lagged state, or predictor state
    :param R: precomputed residual
    :param dt:
    :return:
    '''
    R0, R1, R2 = app.divide(R)

    U_arr = list(app.divide(copy.copy(Um))) #in-place update
    U_lag_arr = app.divide(U_lag)


    print('Solve structure')
    Newton_Solver(app, U_arr, sdc_residual, args=(U_lag_arr, R1, dt, 1))
    print('Solve mesh')
    Linear_Solver(app, U_arr, sdc_residual, args=(U_lag_arr, R2, dt, 2))
    print('Solve fluid')
    Newton_Solver(app, U_arr, sdc_residual, args=(U_lag_arr, R0, dt, 0))


    # print('Solve mesh')
    # Linear_Solver(app, U_arr, sdc_residual, args=(U_lag_arr, R2, dt, 2))
    # print('Solve fluid')
    # Newton_Solver(app, U_arr, sdc_residual, args=(U_lag_arr, R0, dt, 0))
    # print('Solve structure')
    # Newton_Solver(app, U_arr, sdc_residual, args=(U_lag_arr, R1, dt, 1))

    return app.combine(U_arr[0], U_arr[1], U_arr[2])

def sdc_prim_advance(nPoints, nIter, q, ts, app, Qm, tn, dt):
    '''
    :param nPoints:
    :param Qm:
    :param tn: current time corresponding to Qm
    :param dt:
    :param Q:
    :return:
    '''

    uPrev = np.tile(Qm,(nPoints,1))
    uNext = np.tile(Qm,(nPoints,1))

    #also save the residual at each point
    app.set_time(tn)
    Lm = app.velo(Qm)
    lPrev = np.tile(Lm, (nPoints, 1))
    lNext = np.tile(Lm, (nPoints, 1))

    R0 = np.empty(len(Qm))

    for iter in range(nIter):
        for i in range(1, nPoints):
            #solve for structure U1
            #solve for mesh U2
            #solve for fluid U0
            # Compute u_{m + 1} ^ {k + 1}

            dts = (ts[i] - ts[i-1])*dt
#            dts = dt
            # Compute the common part of the residual
            # for all subsystems, set R = 0
            R0[:] = 0
            # add the integration part
            for j in range(nPoints):
                R0 -= dt * q[(i - 1) * nPoints + j] * lPrev[j,:]


            # add M um_{i - 1}^{nite}
            R0 -= app.mass_rmult(uNext[i - 1,:])

            # add dt * F(u_{i } ^ {nite - 1})
            R0 += dts*lPrev[i,:]

            app.set_time(tn + ts[i]*dt)
            uNext[i,:] = sdc_solve(app, uNext[i - 1,:], uPrev[i,:], R0, dts) #todo uPrev[i,:] or uNext[i-1,:]
            lNext[i,:] = app.velo(uNext[i,:])


            # copy to uPrev
        uPrev[:,:] = uNext
        lPrev[:,:] = lNext

    Q = uNext[nPoints - 1,:]
    return Q




