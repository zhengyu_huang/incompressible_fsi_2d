#implement staggered algorithm
import numpy as np
import sys
from scipy.sparse.linalg import spsolve
sys.path.insert(0, '../Apps')
from FSI import Newton_Solver


alpham, alphaf = -1., 0.0


def Newton_Solver(app, U, func, args, alpha = 1.0):
    MAXITE = 100
    eps = 1.e-8
    res0 = np.inf
    for ite in range(MAXITE):
        F, dF = func(app, U, *args)

        dU = -spsolve(dF, F)
        res = np.linalg.norm(F)

        while alpha * res > res0:
            alpha /= 2.0
            print('reduce alpha', alpha)
        U += alpha*dU
        print('Ite = ', ite, '||F|| = ', res)
        if (res < eps):
            break
        alpha = min(1., 2*alpha)
        res0 = res
    return U


def ssa_residual(app, U, C, R, dt):
    M = app.get_mass()
    K, L = app.assembly(U, C)
    res = M.dot(U) - dt * L - R
    dres = M - dt * K
    return res, dres


def ssa_prim_advance(app, Qm, tn, dt, acceleration):
    '''
    :param nPoints:
    :param Qm:
    :param tn: current time corresponding to Qm
    :param dt:
    :param Q:
    :return:
    '''

    #solve mesh, with

    app.set_time(tn + dt)

    Um0, Um1, Um2 = app.divide(Qm)

    Am1, Am2 = acceleration[0:app.app1.nEquations//2], acceleration[app.app1.nEquations//2:] #acceleration

    # solve the mesh, with constant structure predictor
    print('solve the mesh')
    C2 = app.get_coupling_2(Um0, Um1, Um2)
    U2, A2 = app.app2.Newmark(tn, dt, Um2, Am2, C2, alpham, alphaf)



    # solve the fluid with mesh with BE
    print('solve the fluid')
    C0 = app.get_coupling_0(Um0, Um1, U2)
    R0 = app.app0.get_mass().dot(Um0)
    arg = [C0, R0, dt]
    U0 = Newton_Solver(app.app0, Um0, ssa_residual, arg)

    # solve the structure with the load
    print('solve the structure')
    C1 = app.get_coupling_1(U0, Um1, U2)
    U1, A1 = app.app1.Newmark(tn, dt, Um1, Am1, C1, alpham, alphaf)


    return app.combine(U0, U1, U2), np.concatenate((A1, A2))



def ssa_prim_advance_bdf(app, Qm, tn, dt, acceleration):
    '''
    :BDF with second order displacement predictor
    :return:
    '''

    #solve mesh, with

    app.set_time(tn + dt)

    Um0, Um1, Um2 = app.divide(Qm)

    Am1, Am2 = acceleration[0:app.app1.nEquations//2], acceleration[app.app1.nEquations//2:] #acceleration

    # solve the mesh, with constant structure predictor
    print('solve the mesh')
    C2 = app.get_coupling_2(Um0, Um1, Um2)
    U2, A2 = app.app2.Newmark(tn, dt, Um2, Am2, C2, alpham, alphaf)



    # solve the fluid with mesh with BE
    print('solve the fluid')
    C0 = app.get_coupling_0(Um0, Um1, U2)
    R0 = app.app0.get_mass().dot(Um0)
    arg = [C0, R0, dt]
    U0 = Newton_Solver(app.app0, Um0, ssa_residual, arg)

    # solve the structure with the load
    print('solve the structure')
    C1 = app.get_coupling_1(U0, Um1, U2)
    U1, A1 = app.app1.Newmark(tn, dt, Um1, Am1, C1, alpham, alphaf)


    return app.combine(U0, U1, U2), np.concatenate((A1, A2))


