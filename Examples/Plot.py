import numpy as np
import matplotlib.pyplot as plt
import copy
import sys

sys.path.insert(0, '../Apps')
sys.path.insert(0, '../Integrators')

from FSI import FSI
from Structure import Structure
from Beam import Beam
from IncompressibleFluid import IncompressibleFluid
from MeshGen import mesh_gen
import SDC

def set_app(Nx_, ngp_, pref='bin_3_400_weak_sdc/'):
    ########### Fluid App
    rho_f, nu_f = 1., 0.01
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen(
        'Modified_Cavity_Fluid', Nx_, Nx_, ngp_)
    app0 = IncompressibleFluid(Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray,
                               nElements, nNodes, nEquations, rho_f, nu_f)
    wettedNodesF = np.arange(0, 2 * Nx + 1, dtype=int)
    wettedEdgesF = np.zeros((Nx, 2), dtype=int)
    wettedEdgesF[:, 0] = np.arange(Nx)
    app0.set_wetted_surface(wettedNodesF, wettedEdgesF)

    ########### Nonlinear Structure App
    rho_s, E_s, nu_s, A_s = 400., 250., 0., 0.002
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen(
        'Modified_Cavity_Beam', Nx_, 1, ngp_)
    app1 = Beam(Nx, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray,
                              nElements, nNodes, nEquations, rho_s, E_s, A_s)

    wettedNodesS = [(0, -1.0)]
    for i in range(Nx):
        # attention, r in [-1, 1]
        wettedNodesS.append((i, 0.0))
        wettedNodesS.append((i, 1.0))

    app1.set_wettedNodes(wettedNodesS)

    ########### Linear Structure Mesh App
    rho_m, E_m, nu_m = 500, 250, 0.0
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen(
        'Modified_Cavity_Mesh', Nx_, Nx_, ngp_)
    app2 = Structure(Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements,
                     nNodes, nEquations, rho_m, E_m, nu_m)
    wettedNodesM = np.arange(0, 2 * Nx + 1)
    app2.set_DirichletNodes(wettedNodesM)


    ########### Threeway coupled FSI app
    app = FSI(app0, app1, app2)

    app.pref = pref

    return app

def visualize(Nx_, n = 0, ngp_ = 3):
    app = set_app(Nx_, ngp_)
    app.set_time( n * 0.1)
    U = np.load(app.pref + str(n).zfill(5) + '.dat.npy')
    app.visualize_data(U)
    plt.show()


def make_movie(Nx_, ngp_ = 3):
    app = set_app(Nx_, ngp_, pref='bin_SDC2_500/')

    for n in range(998, -1, -2):
        app.set_time( n * 0.1)
        U = np.load(app.pref + str(n).zfill(5) + '.dat.npy')

        U0, U1, U2 = app.divide(U)


        C0 = app.get_coupling_0(U0, U1, U2)

        app.app0.visualize(U0, C0, "pressure")

        #plt.show()

        plt.savefig('modified_cavity_2_500_' + str(n).zfill(5)+".png", bbox_inches='tight')



def visualize_disp_forces(Nx_, ngp_=3, pref='bin_2_500_weak_sdc/'):
    #Visualize y-direction forces of node in nodes

    app = set_app(Nx_, ngp_)
    #change
    prefs  = ['bin_BE_800/']#, 'bin_SDC2_500/']#, 'bin_2_500_weak_sdc/', 'bin_3_400_weak_sdc/']
    labels = ['bin_SDC2_500/',  'bin_2_500_weak_sdc/', 'bin_2_500_weak_sdc/', 'ddd']#, 'bin_2_500_weak_sdc/', 'bin_2_500_weak_sdc/']

    dt = 0.1
    nf = Nx_
    ns = Nx_//2
    frames = np.arange(0, 1000, 2)
    for id, pref in enumerate(prefs):
        app.pref = pref
        forcesy, dispy = np.empty((len(frames))),np.empty((len(frames)))
        for i in range(len(frames)):


            n = frames[i]
            app.set_time( n * dt)
            U = np.load(app.pref + str(n).zfill(5) + '.dat.npy')
            C1 = app.get_force(U)

            _,U1,_ = app.divide(U)

            U1_all = app.app1.get_full_field(U1)
            dispy[i] = U1_all[ns, 4]
            forcesy[i] = C1[nf, 1]

        plt.figure(1)    
        plt.plot(frames * dt, forcesy, '-*r' , label = labels[id])

        plt.figure(2)
        plt.plot(frames * dt, dispy, '-*b', label=labels[id])
    plt.show()


def visualize_disp_all(Nx_, ngp_=3):
    #Visualize y-direction forces of node in nodes
    plt.figure(figsize=(12.8, 6.0))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    app = set_app(Nx_, ngp_)

    prefs  = ['bin_BE_900/', 'bin_SDC1_1200/', 'bin_SDC2_500/', 'bin_SDC3_800/', 'bin_SDC3r_400/', 'bin_SDC4_1000/']
    mass = [900, 1200, 500, 800, 400, 1000]
    markers = ['-D', '-*', '-o', '-s', '-^', '-X']
    colors = ['saddlebrown', 'orange', 'red', 'springgreen', 'blue', 'black']
    names =  ['BE', 'SDC1', 'SDC2', 'SDC3-l', 'SDC3-r', 'SDC4']
    dt = 0.1
    nf = Nx_
    ns = Nx_//2
    frames = np.arange(0, 1000, 2)
    for id, pref in enumerate(prefs):
        app.pref = pref
        dispy = np.empty((len(frames)))
        for i in range(len(frames)):


            n = frames[i]
            app.set_time( n * dt)
            U = np.load(app.pref + str(n).zfill(5) + '.dat.npy')
            C1 = app.get_force(U)

            _,U1,_ = app.divide(U)

            U1_all = app.app1.get_full_field(U1)
            dispy[i] = U1_all[ns, 4]
          


       
        plt.plot(frames * dt, dispy, markers[id], color=colors[id], label=r'$\rho^s$=%d, %s' %(mass[id], names[id]), markevery=5)
       

            
    mysize = 20
    plt.xlabel('Time',size=mysize, labelpad=2)
    plt.ylabel('Displacement',size=mysize, labelpad=2)
    plt.tick_params(axis='both', labelsize=mysize)
    plt.tick_params(axis='both', labelsize=mysize)

    plt.legend(prop={'size': mysize})
    plt.tight_layout()
    plt.show()




if __name__ == '__main__':
    Nx_ = 32
    T = 100
    Nt = 1000
    #radau lobatto
  
    #frames = np.arange(0, 129, 2)
    #nodes = np.array([Nx_ // 2, Nx_, Nx_ * 3 // 2], dtype=int)
    #nodes = np.array([Nx_], dtype=int)
    #visualize_forces(Nx_, frames, nodes)

    #visualize_disp_forces(Nx_)

    visualize_disp_all(Nx_)

    #make_movie(Nx_)
    #visualize(Nx_, n=128, ngp_=3)
