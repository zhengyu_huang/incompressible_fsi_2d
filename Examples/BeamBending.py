import numpy as np
import sys
from scipy.sparse.linalg import spsolve
sys.path.insert(0, '../Apps')

from Beam import Beam
from MeshGen import mesh_gen
from FSI import Newton_Solver
import copy

ngp_ = 4
Nx_ = 32
rho_s, E_s, A_s = 500., 250., 0.002
Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen(
      'Modified_Cavity_Beam', Nx_, 1, ngp_)
app1 = Beam(Nx, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray,
                          nElements, nNodes, nEquations, rho_s, E_s, A_s)

wettedNodesS = [(0,-1.0)]
for i in range(Nx):
    # attention, r in [-1, 1]
    wettedNodesS.append((i, 0.0))
    wettedNodesS.append((i, 1.0))

app1.set_wettedNodes(wettedNodesS)

def derivative_test():
    U = np.random.normal(1.0, 0.1, app1.nEquations)


    C0 = np.random.normal(1.0, 0.1, (len(wettedNodesS), 2))  # C0.fill(-1.e-5)
    C = [C0, None]

    unit = np.random.normal(1.0, 0.1, app1.nEquations)

    eps = 1.e-4
    U_p = U + eps * unit
    U_m = U - eps * unit


    K, L     = app1.assembly(U,   C)
    K_p, L_p = app1.assembly(U_p, C)
    K_m, L_m = app1.assembly(U_m, C)

    error = (L_p - L_m) / (2 * eps) - np.dot(K, unit)

    print(error)


def static_test():
    U = np.zeros(app1.nEquations)
    C0 = np.zeros((len(wettedNodesS), 2)) #C0.fill(-1.e-5)
    C0[:, 1] = 1.e-3
    MAXITE = 100
    eps = 1.e-6
    LOADSTEPPING = 1
    for iteLoadstep in range(1,LOADSTEPPING+1):
        C = float(iteLoadstep)/float(LOADSTEPPING) * C0

        for ite in range(MAXITE):
            K,L = app1.assembly(U, C)
            dU = -np.linalg.solve(K, L)
            res = np.linalg.norm(dU)
            print('Load Stepping Ite = ', iteLoadstep, ' Ite = ', ite, ' ||dU|| = ', res )
            if(res < eps):
                break
            U += dU


    app1.visualize(U)

def dynamic_test():
    Um = np.zeros(app1.nEquations)
    C0 = np.zeros((len(wettedNodesS), 2));
    # C0[0,:] = -1.e-6
    C0[:, 1] = 1.e-3
    C = C0


    dt, Nt  = 0.01, 100

    MAXITE = 100
    eps = 1.e-6

    U = copy.copy(Um)
    M = app1.get_mass()

    for i in range(Nt):

        for ite in range(MAXITE):
            K, L = app1.assembly(U, C)
            F = np.dot(M, U - Um) - dt*L
            dF = M - dt*K
            dU = -np.linalg.solve(dF, F)
            res = np.linalg.norm(F)
            print(' Ite = ', ite, ' ||F|| = ', res )
            if(res < eps):
                break
            U += dU
        Um = copy.copy(U)

    app1.visualize(U, True)



def dynamic_Newmark_test():
    Um = np.zeros(app1.nEquations)
    C0 = np.zeros((len(wettedNodesS), 2))
    alpham ,alphaf = -1., 0.0
    #C0[0,:] = -1.e-6
    C0[:,1] = 1.e-3
    C = C0



    M = app1.get_mass()
    dt, Nt  = 0.01, 100
    R1 = - M.dot(Um)
    t_n = 0.0
    a_n =  np.zeros(app1.nEquations//2)
    for i in range(Nt):
        Um, a_n = app1.Newmark(t_n, dt, Um, a_n, C, alpham, alphaf)

        t_n += dt

    app1.visualize(Um, True)

if __name__ == '__main__':

    #static_test()
    dynamic_Newmark_test()
    dynamic_test()
