import numpy as np
import sys
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as plt
import multiprocessing
import pickle
import os


sys.path.insert(0, '../Apps')

import IncompressibleFluid
from MeshGen import build_mesh, build_IEN, build_ID
import copy




def Spline(x, theta):
    #
    # x in [x_l, x_h], piecewise cubic polynomial
    # o --- o --- o ...... o --- o
    # 1     3                    N_theta-1=2ne+1
    # 2     4                    N_theta  =2ne+2
    # 
    # fix the starting point
    theta[0] = 0.0
    
    x_l, x_h = x[0], x[-1]
    nx = len(x)
    N_e = (len(theta) - 2) // 2
    delta_e = (x_h - x_l)/N_e
    int_val = np.zeros(nx,dtype = np.int64)
    int_val[:] = np.floor((x - x_l)/delta_e)
    int_val[-1] = N_e-1
    

    xi = (x - x_l - delta_e*(int_val) )/delta_e
    

    N1 = (1 - 3*xi**2 + 2*xi**3)  # 0  -> 1  
    N2 = (xi - 2*xi**2 + xi**3)   # 0' -> 1
    N3 = (3*xi**2 - 2*xi**3)      # 1  -> 1
    N4 = (-xi**2 + xi**3)       # 1' -> 1
    

    deformation =  N1*theta[2*int_val] + N2*theta[2*int_val+1] + N3*theta[2*int_val+2] + N4*theta[2*int_val+3]
    
    return deformation


def PipePlot(cnx = 128, cny = 64):
    N_theta = 6
    
    theta = 2*np.random.rand(2*N_theta) - 1
    
    dmax = 2.0
    # displacement
    theta[1::2] *= dmax
    gmax = 1.0
    # gradient (tan)
    theta[2::2] *= gmax

    # cnx, cny = 128, 64 #cells
    Nx, Ny = 2*cnx+1, 2*cny+1 #points

    # Construct uniform grid
    Lx, Ly = 10.0, 1.0
    x = np.linspace(0, Lx, Nx)
    y = np.linspace(-Ly/2.0, Ly/2.0, Ny)

    # Deform grid
    deformation = Spline(x, theta)

    xx = np.zeros((Nx, Ny))
    yy = np.zeros((Nx, Ny))
    for i in range(Nx):
        for j in range(Ny):
            xx[i,j] = x[i]
            yy[i,j] = y[j] + deformation[i]


    plt.pcolormesh(xx, yy, np.zeros((Nx, Ny)), facecolor="none", edgecolor="r")
    plt.plot(x, deformation, "-o")
    plt.show()

# theta 12 values uniform in [-1, 1]
def Pipe(theta, cnx = 32, cny = 16, nu_f = 0.005):
    
    # N_theta = 6
    # theta = 2*np.random.rand(2*N_theta) - 1
    dmax = 2.0
    # displacement
    theta[1::2] *= dmax
    gmax = 1.0
    # gradient (tan)
    theta[2::2] *= gmax

    
    ngp = 3
    rho_f  = 1.0

    elementOrder, elementType = 2, 'Q2Q1'
    
    Nx, Ny = elementOrder*cnx + 1,  elementOrder*cny + 1
    
    nNodesElement = 9
    nDoF = 3
    nDofsArray = np.array([(elementOrder+1)**2, (elementOrder+1)**2, (1+1)**2], dtype=int)

    Lx_0, Lx_1, Ly_0, Ly_1 = 0.0, 10.0, -0.5, 0.5
    yy = np.linspace(Ly_0, Ly_1, Ny)
    xx = np.linspace(Lx_0, Lx_1, Nx)
    nElements, nNodes, Coord = build_mesh(cnx, cny, Lx_0, Ly_0, Lx_1, Ly_1, elementOrder)

    # update Coord
    X = np.reshape(Coord[0, :] , (2*cnx+1, 2*cny+1), order='F')
    Y = np.reshape(Coord[1, :] , (2*cnx+1, 2*cny+1), order='F')
    
    
    
    
    deformation = Spline(xx, theta)
    Y += np.outer(deformation, np.ones(Ny))
    Coord[1, :] = Y.flatten(order='F')
    

    IEN = build_IEN(cnx, cny, 2)
    #########################################################
    EBC = np.zeros((nNodes, nDoF), dtype=int)
    g = np.zeros((nNodes, nDoF), dtype=float)

    # Top wall
    EBC[2*cny*(2*cnx + 1): (2*cny + 1)*(2*cnx + 1), 0:2] = 1
    g[2*cny*(2*cnx + 1): (2*cny + 1)*(2*cnx + 1), 0:2] = 0.

    # Bottom wall
    EBC[0: (2 * cnx + 1), 0:2] = 1
    g[0: (2 * cnx + 1), 0:2] = 0

    # Left inlet
    EBC[0:2*cny* (2*cnx + 1)+1: (2*cnx + 1), 0:2] = 1
    U_max = 1.0
    g[0:2*cny * (2 * cnx + 1)+1: (2 * cnx + 1), 0] = U_max*(0.5 - yy) * (0.5 + yy)
    g[0:2*cny * (2 * cnx + 1)+1: (2 * cnx + 1), 1] = 0.


    # Fix pressure at the outlet center
    # EBC[2*cnx + cny * (2 * cnx + 1), 2] = 1
    # g[2*cnx+ cny * (2 * cnx + 1), 2] = 0.
    
    # Fix pressure at the inlet center
    # EBC[cny * (2 * cnx + 1), 2] = 1
    # g[cny * (2 * cnx + 1), 2] = 0.

    # Right outlet (free boundary condition)
    NBC = np.zeros((nElements, 4), dtype=int)
    NBC[cnx-1:cny*cnx:cnx, 1] = 2

        
    nEquations, ID = build_ID(cnx, cny, nDoF, EBC, elementOrder, elementType)
 
    problemType = "Pipe"
    app0 = IncompressibleFluid.IncompressibleFluid(cnx, cny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray,
                                nElements, nNodes, nEquations, rho_f, nu_f)

    U0 = np.zeros(app0.nEquations)
    C0 = np.zeros(4 * app0.nNodes)

    U = copy.copy(U0)

    MAXITE = 10
    eps = 1.e-10
    for ite in range(MAXITE):
        K,L = app0.assembly(U, C0)
        dU = -spsolve(K, L)

        res = np.linalg.norm(L)
        print('Ite = ', ite, '||F|| = ', res )
        if(res < eps):
            break
        U += dU
    

    # app0.visualize(U, C0, "pressure")
    U_all = app0.get_full_field(U, C0) #This case no mesh motion, g = 1
    ux, uy , p =  np.reshape(U_all[:,0], (2*cnx+1, 2*cny+1), order='F'),  np.reshape(U_all[:,1], (2*cnx+1, 2*cny+1), order='F'), np.reshape(U_all[:,2], (2*cnx+1, 2*cny+1), order='F')
    print(X.shape, ux.shape)
    plot_or_not = False
    if plot_or_not:
        fig, ax = plt.subplots(3, 1, figsize=(6, 6), dpi=80)
        im0 = ax[0].pcolormesh(X, Y, ux, shading='gouraud')
        fig.colorbar(im0, ax = ax[0])
        ax[0].set_title("u")
        ax[0].axis('equal')
        

        im1 = ax[1].pcolormesh(X, Y, uy, shading='gouraud')
        fig.colorbar(im1, ax = ax[1])
        ax[1].set_title("v")
        ax[1].axis('equal')
        

        im2 = ax[2].pcolormesh(X[0::2,0::2], Y[0::2,0::2], p[0::2,0::2], shading='gouraud')
        fig.colorbar(im2, ax = ax[2])
        ax[2].set_title("p")
        ax[2].axis('equal')
        
        fig.tight_layout()
        fig.show()

    # p -= p[2*cnx,  0]
    return  X, Y, ux, uy, p, res

def GenerateData():
    N_data = 2500
    N_theta = 5
    np.random.seed(42)
    thetas = 2*np.random.rand(N_data, 2*N_theta) - 1

    cnx, cny = 64,  64
    Nx, Ny = 2*cnx+1, 2*cny+1
    
    X_data, Y_data = np.zeros((N_data, Nx, Ny)), np.zeros((N_data, Nx, Ny))
    Q_data,  res_data = np.zeros((N_data, 3, Nx, Ny)), np.zeros((N_data, 1))
    pool = multiprocessing.Pool(min(32, multiprocessing.cpu_count()))
    results = []
    for i in range(N_data):
        results.append(pool.apply_async(Pipe, (thetas[i,:], cnx, cny)))
        
    for (i, result) in enumerate(results):
        X_data[i,:,:], Y_data[i,:,:], Q_data[i,0,:,:], Q_data[i,1,:,:], Q_data[i,2,:,:], res_data[i] = result.get()
        
        
    pool.close()
     
    np.save("Pipe_X.npy", X_data)
    np.save("Pipe_Y.npy", Y_data)
    np.save("Pipe_Q.npy", Q_data)
    np.save("Pipe_res.npy", res_data)
    


def TimeGenerateData():
    N_data = 10
    N_theta = 5
    np.random.seed(42)
    thetas = 2*np.random.rand(N_data, 2*N_theta) - 1

    cnx, cny = 64,  64
    Nx, Ny = 2*cnx+1, 2*cny+1
    
    X_data, Y_data = np.zeros((N_data, Nx, Ny)), np.zeros((N_data, Nx, Ny))
    Q_data,  res_data = np.zeros((N_data, 3, Nx, Ny)), np.zeros((N_data, 1))
    
    for i in range(N_data):
        Pipe(thetas[i,:], cnx, cny)

GenerateData()        
# PipePlot()
# import time

# start = time.time()
# TimeGenerateData()
# end = time.time()
# print(end - start)

