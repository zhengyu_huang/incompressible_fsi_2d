import numpy as np
import matplotlib.pyplot as plt
import copy
import sys

sys.path.insert(0, '../Apps')
sys.path.insert(0, '../Integrators')

from FSI import FSI
from Ode3 import Ode3
import SDC

def exact_sol(K, A, rho, x0, T):
    '''
    rho * dx/dt = Kx + At
    :param K: 3 by 3 matrix
    :param A: diagonal matrix, represented by a 1 by 3 array
    :param rho: diagonal matrix, represented by a 1 by 3 array
    :return:

    dy/dt = a y + bt with y(0) = y0
    y = (y0 + b/a^2)e^(at) - b/a*t - b/a^2
    '''

    Kp = K/rho[:,None]
    Ap = A/rho
    # solve dx/dt = Kpx + Apt
    Sigma, P = np.linalg.eig(Kp)
    # solve dx/dt = P Sigma P^{-1} x + Apt
    # let y = P^{-1}x
    # dy/dt = Sigma y * P^{-1} Ap t

    Pinv = np.linalg.inv(P)
    B = np.dot(Pinv, Ap)
    y0 = np.dot(Pinv, x0)

    # y = np.empty(3)
    # for i in range(3):
    #     y[i] = (y0[i] + B[i]/Sigma[i]**2)*np.exp(Sigma[i]*T) - B[i]/Sigma[i]*T - B[i]/Sigma[i]**2
    #y = (y0) * np.exp(Sigma * T)

    y = (y0 + B/Sigma**2)*np.exp(Sigma*T) - B/Sigma*T - B/Sigma**2

    x = np.dot(P, y)

    return x

def sdc_solve(K, A, rho, x0, T, Nt, sdc_type):

    dt = np.empty(Nt)

    dt.fill(float(T)/Nt)

    app0 = Ode3(rho[0], K, A, 0)
    app1 = Ode3(rho[1], K, A, 1)
    app2 = Ode3(rho[2], K, A, 2)
    app = FSI(app0, app1, app2)


    q, ts, nPoints, nIter = SDC.sdc_method_coefficients(sdc_type)

    t = 0.0
    Um = copy.copy(x0)
    for n in range(Nt):
        if (n % 50 == 0):
            print("=== Primal Timestep ", n ," ===")

        U = SDC.sdc_prim_advance(nPoints, nIter, q, ts, app, Um, t, dt[n])

        # update for next timestep
        t += dt[n]
        Um[:] = U

    return U


def imex_solve(K, A, rho, x0, T, Nt, imex_type):

    dt = np.empty(Nt)

    dt.fill(float(T)/Nt)

    app0 = Ode3(rho[0], K, A, 0)
    app1 = Ode3(rho[1], K, A, 1)
    app2 = Ode3(rho[2], K, A, 2)
    app = FSI(app0, app1, app2)


    nStages, Ae, be, ce, Ai, bi, ci = IMEX.imex_method_coefficients(imex_type)

    t = 0.0
    Um = copy.copy(x0)
    for n in range(Nt):
        if (n % 50 == 0):
            print("=== Primal Timestep ", n ," ===")

        U = IMEX.imex_prim_advance(nStages, Ae, be, ce, Ai, bi, ci, app, Um, t, dt[n])

        # update for next timestep
        t += dt[n]
        Um[:] = U

    return U


def error_plot():
    K = np.array([[1., 1., 2.],
                  [2., 2., 3.],
                  [0., 1., 1.]])
    A = np.array([1., 2., 3.])
    rho = np.array([1., 1., 1.])


    T = 2.0

    x0 = np.array([1., 2., 0.])
    U_exact = exact_sol(K, A, rho, x0, T)
    print('U_exact is ', U_exact)

    Nc = 5
    errors, axis_dt = np.empty(Nc), np.empty(Nc)
    Nt = 20


    test_list = [('radau1', 1), ('lobatto2', 2), ('radau3', 3), ('lobatto4', 4)]
    errors = np.empty((Nc, len(test_list)))
    for test_id in range(len(test_list)):
        Nt = 20
        sdc_type, order  = test_list[test_id]
        print("============================ ", sdc_type, " ===============================================")
        for i in range(Nc):
            U_sdc = sdc_solve(K, A, rho, x0, T, Nt, sdc_type)

            errors[i, test_id] = np.linalg.norm(U_sdc - U_exact)

            axis_dt[i] = float(T) / Nt
            Nt *= 2

    f = open('ode3_GS.dat', 'w')
    f.write('dt   sdc1   sdc2  sdc3  sdc4\n')
    for i in range(Nc):
        f.write('%.12E   %.12E   %.12E  %.12E  %.12E\n' %(axis_dt[i], errors[i,0], errors[i,1], errors[i,2], errors[i,3]))  # python will convert \n to os.linesep
    f.close()  # you can omit in most cases as the destructor will call it


if __name__ == '__main__':
    error_plot()

    K = np.array([[1., 1., 2.],
                  [2., 2., 3.],
                  [0., 1., 1.]])
    A = np.array([1., 2., 3.])
    rho = np.array([1., 1., 1.])

    # K = np.array([[1., 1., 1.],
    #               [1., 1., 1.],
    #               [0., 1., 1.]])
    # A = np.array([0., 0., 0.])
    #
    # rho = np.array([1., 1., 1.])






    T = 2.0

    x0 = np.array([1.,2.,0.])
    U_exact = exact_sol(K, A, rho, x0, T)
    print('U_exact is ', U_exact)






    Nc = 5
    errors, axis_dt =  np.empty(Nc), np.empty(Nc)
    Nt = 20

    test = 'SDC'


    sdc_type, order = 'radau5', 5
    for i in range(Nc):

        U_sdc = sdc_solve(K, A, rho, x0, T, Nt, sdc_type)

        errors[i] = np.linalg.norm(U_sdc - U_exact)

        axis_dt[i] = float(T)/Nt

        Nt *= 2


    print(errors)
    errors_ref = axis_dt**order
    base = 2
    plt.figure(1)
    plt.loglog(axis_dt, errors, 'o-', basex=base, basey=base, label="error")
    plt.loglog(axis_dt, errors_ref, 'k--', basex=base, basey=base, label="O(h^%d)" % (order))
    plt.grid(True)
    plt.xlabel('dt')
    plt.ylabel('error')
    plt.legend(loc='upper left')
    plt.title('Temporal Error')
    plt.show()




