import numpy as np
import matplotlib.pyplot as plt
import copy
import sys

sys.path.insert(0, '../Apps')
sys.path.insert(0, '../Integrators')

from FSI import FSI
from Structure import Structure
from Beam import Beam
from IncompressibleFluid import IncompressibleFluid
from MeshGen import mesh_gen
import SDC

def set_app(Nx_, ngp_, pref='bin_SDC2_500/'):
    ########### Fluid App
    rho_f, nu_f = 1., 0.01
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen(
        'Modified_Cavity_Fluid', Nx_, Nx_, ngp_)
    app0 = IncompressibleFluid(Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray,
                               nElements, nNodes, nEquations, rho_f, nu_f)


    wettedNodesF = np.arange(0, 2 * Nx + 1, dtype=int)
    wettedEdgesF = np.zeros((Nx, 2), dtype=int) # pair(element_id, local_face_id) ,local face id : bottom, right, top, left
    wettedEdgesF[:, 0] = np.arange(Nx)
    app0.set_wetted_surface(wettedNodesF, wettedEdgesF)

    ########### Nonlinear Structure App
    rho_s, E_s, nu_s, A_s = 500., 250., 0., 0.002
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen(
        'Modified_Cavity_Beam', Nx_, 1, ngp_)
    app1 = Beam(Nx, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray,
                              nElements, nNodes, nEquations, rho_s, E_s, A_s)

    wettedNodesS = [(0, -1.0)] # pair(element_id, local_coordinate of matched nodes)
    for i in range(Nx):
        # attention, r in [-1, 1]
        wettedNodesS.append((i, 0.0))
        wettedNodesS.append((i, 1.0))

    app1.set_wettedNodes(wettedNodesS)

    ########### Linear Structure Mesh App
    rho_m, E_m, nu_m = 500, 250, 0.0
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen(
        'Modified_Cavity_Mesh', Nx_, Nx_, ngp_)
    app2 = Structure(Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements,
                     nNodes, nEquations, rho_m, E_m, nu_m)
    wettedNodesM = np.arange(0, 2 * Nx + 1)
    app2.set_DirichletNodes(wettedNodesM)


    ########### Threeway coupled FSI app
    app = FSI(app0, app1, app2)

    app.pref = pref


    return app

def visualize(Nx_, n = 0, ngp_ = 3):
    app = set_app(Nx_, ngp_)
    app.set_time( n * 0.1)
    U = np.load(app.pref + str(n).zfill(5) + '.dat.npy')
    app.visualize_data(U)



def sdc_solve(Nx_, T, Nt, sdc_type, ngp_ = 3, restart_id = -1):
    '''

    :param N: Fluid mesh N by N, structure mesh N by 1
    :param T: Time
    :param Nt: time step
    :param sdc_type:
    :return:
    '''
    dt = np.empty(Nt); dt.fill(float(T)/Nt)

    app = set_app(Nx_, ngp_)
    ########### Initial condition
    x0 = np.zeros(app.app0.nEquations + app.app1.nEquations + app.app2.nEquations)


    q, ts, nPoints, nIter = SDC.sdc_method_coefficients(sdc_type)

    t = 0.0

    if restart_id > 0:
        Um = np.load(app.pref + str(restart_id).zfill(5) + '.dat.npy')
        t = restart_id * dt[0]
        print('restart from time ', t, ' file ', app.pref + str(restart_id).zfill(5) + '.dat.npy')
    else:
        Um = copy.copy(x0)


    for n in range(restart_id + 1, Nt):

        print("=== Primal Timestep ", n ," ===")

        U = SDC.sdc_prim_advance(nPoints, nIter, q, ts, app, Um, t, dt[n])

        # update for next timestep
        t += dt[n]
        Um[:] = U

        if n%2 == 0:
            print('save data')
            app.save_data(U, n)


    return U


if __name__ == '__main__':
    Nx_ = 32
    T = 100
    Nt = 1000
    #radau lobatto
    sdc_type = 'lobatto2'
    sdc_solve(Nx_, T, Nt, sdc_type, 4)

    nframe = 100
    visualize(Nx_, nframe)
