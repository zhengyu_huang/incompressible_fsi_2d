import numpy as np
import matplotlib.pyplot as plt
import copy
import sys
'''
Solve du/dt = A u
A = [0 ]
'''

sys.path.insert(0, '../Apps')
sys.path.insert(0, '../Integrators')


import SDC

def exact_sol(U0, alpha, t):
    U_all = np.zeros((len(t), 2))
    U_all[:, 0] = U0[0] * (-1.0 / (alpha - 1.0) * np.exp(-alpha*t) + alpha/(alpha - 1)*np.exp(-t))
    U_all[:, 1] = U0[0] * (alpha / (alpha - 1.0) * np.exp(-alpha * t) - alpha / (alpha - 1) * np.exp(-t))
    return U_all

def sdc_solve(alpha, U0, T, Nt, sdc_type):

    A = np.array([[0.0,1.0],[-alpha, -alpha-1]])

    dt = float(T)/Nt

    q, ts, nPoints, nIter = SDC.sdc_method_coefficients(sdc_type)

    U_all = np.zeros((Nt + 1, 2))
    U_all[0,:] = U0

    Um = copy.copy(U0)
    for n in range(Nt):
        if (n % 50 == 0):
            print("=== Primal Timestep ", n ," ===")

        uPrev = np.tile(Um, (nPoints, 1))
        uNext = np.tile(Um, (nPoints, 1))

        # also save the residual at each point
        Lm = np.dot(A , Um)
        lPrev = np.tile(Lm, (nPoints, 1))
        lNext = np.tile(Lm, (nPoints, 1))

        R0 = np.empty(len(Um))

        for iter in range(nIter):
            for i in range(1, nPoints):
                # solve for U0
                # solve for U1

                dts = (ts[i] - ts[i - 1]) * dt
                if sdc_type == "radau3-r":
                    dts = dt


                # Compute the common part of the residual
                # for all subsystems, set R = 0
                R0[:] = 0
                # add the integration part
                for j in range(nPoints):
                    R0 -= dt * q[(i - 1) * nPoints + j] * lPrev[j, :]

                # add M um_{i - 1}^{nite}
                R0 -= uNext[i - 1, :]

                # add dt * F(u_{i } ^ {nite - 1})
                R0 += dts * lPrev[i, :]


                uNext[i, 0] = -(R0[0] - dts*A[0,1]*uPrev[i, 1]) /(1.0 - dts*A[0,0])
                uNext[i, 1] = -(R0[1] - dts*A[1,0]*uNext[i, 0]) /(1.0 - dts*A[1,1])


                lNext[i, :] = np.dot(A,  uNext[i, :])

                # copy to uPrev
            uPrev[:, :] = uNext
            lPrev[:, :] = lNext


        Um[:] = uNext[nPoints - 1, :]
        U_all[n+1,:] = Um

    return U_all





def error_plot():
    alpha = 1000.0


    T = 20.0

    ts = np.linspace(0,T, 11)

    U0 = np.array([alpha, 0.])


    U_exact = exact_sol(U0, alpha, ts)
    print('U_exact is ', U_exact)

    Nc = 15
    errors, axis_dt = np.empty(Nc), np.empty(Nc)
    Nt = 20


    test_list = [('radau1', 1), ('lobatto2', 2), ('radau3', 3), ('radau3-r', 3), ('lobatto4', 4)]
    errors = np.empty((Nc, len(test_list)))
    for test_id in range(len(test_list)):
        Nt = 20
        sdc_type, order  = test_list[test_id]
        print("============================ ", sdc_type, " ===============================================")
        for i in range(Nc):
            U_sdc = sdc_solve(alpha, U0, T, Nt, sdc_type)

            errors[i, test_id] = np.linalg.norm(U_sdc[-1,:] - U_exact[-1,:], np.inf)

            axis_dt[i] = float(T) / Nt
            Nt *= 2

    f = open('ode3_GS.dat', 'w')
    f.write('dt   sdc1   sdc2  sdc3  sdc3-r  sdc4\n')
    for i in range(Nc):
        f.write('%.16E   %.16E   %.16E  %.16E  %.16E   %.16E\n' %(axis_dt[i], errors[i,0], errors[i,1], errors[i,2], errors[i,3], errors[i,4]))  # python will convert \n to os.linesep
    f.close()  # you can omit in most cases as the destructor will call it



def solution_plot():
    alpha = 1000.0


    T = 20.0

    ts = np.linspace(0,T, 10000000)

    U0 = np.array([alpha , 0.])


    U_exact = exact_sol(U0, alpha, ts)

    U_sdc = []


    test_list = [('radau1', 1), ('lobatto2', 2), ('radau3', 3), ('radau3-r', 3), ('lobatto4', 4)]
    colors = ['orange', 'red', 'springgreen', 'blue', 'black']
    names = ['SDC1', 'SDC2', 'SDC3-l', 'SDC3-r', 'SDC4']
    markers = ['-D', '-*', '-o', '-s', '-^', '-X']
    Nt = 20
    ts_sdc = np.linspace(0, T, Nt+1)
    for id in range(len(test_list)):

        sdc_type, order  = test_list[id]
        U_sdc.append(sdc_solve(alpha, U0, T, Nt, sdc_type))



    plt.figure(figsize=(8, 6.0))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.plot(ts, U_exact[:,0], label="Exact")
    for id in range(len(test_list)):
        plt.plot(ts_sdc, U_sdc[id][:,0], markers[id], color=colors[id], label=r'%s' %(names[id]), markevery=1)

    mysize = 20
    plt.xlabel('Time', size=mysize, labelpad=2)
    plt.ylabel(r'$u^1$', size=mysize, labelpad=2)
    plt.tick_params(axis='both', labelsize=mysize)
    plt.tick_params(axis='both', labelsize=mysize)

    plt.legend(prop={'size': mysize}, loc="best")
    plt.tight_layout()
    plt.show()

    #############################################
    plt.figure(figsize=(8, 6.0))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.plot(ts, U_exact[:,1], label="Exact")
    for id in range(len(test_list)):
        plt.plot(ts_sdc, U_sdc[id][:, 1], markers[id], color=colors[id], label=r'%s' % (names[id]), markevery=1)

    mysize = 20
    plt.xlabel('Time', size=mysize, labelpad=2)
    plt.ylabel(r'$u^2$', size=mysize, labelpad=2)
    plt.tick_params(axis='both', labelsize=mysize)
    plt.tick_params(axis='both', labelsize=mysize)

    plt.legend(prop={'size': mysize}, loc="best")
    plt.tight_layout()
    plt.show()




if __name__ == '__main__':
    error_plot()
    #solution_plot()
    error()

    alpha = 1000.0

    T = 20.0

    ts = np.linspace(0, T, 11)

    U0 = np.array([alpha, 0.])

    U_exact = exact_sol(U0, alpha, ts)
    print('U_exact is ', U_exact)

    Nc = 15
    errors, axis_dt = np.empty(Nc), np.empty(Nc)
    Nt = 20

    test_list = [('radau1', 1), ('lobatto2', 2), ('radau3', 3), ('radau3-r', 3), ('lobatto4', 4)]
    sdc_type, order = test_list[4]

    for i in range(Nc):

        U_sdc = sdc_solve(alpha, U0, T, Nt, sdc_type)

        errors[i] = np.linalg.norm(U_sdc[-1,:] - U_exact[-1,:])

        axis_dt[i] = float(T)/Nt

        Nt *= 2


    print(errors)
    errors_ref = axis_dt**order
    base = 2
    plt.figure(1)
    plt.loglog(axis_dt, errors, 'o-', basex=base, basey=base, label="error")
    plt.loglog(axis_dt, errors_ref, 'k--', basex=base, basey=base, label="O(h^%d)" % (order))
    plt.grid(True)
    plt.xlabel('dt')
    plt.ylabel('error')
    plt.legend(loc='upper left')
    plt.title('Temporal Error')
    plt.show()




