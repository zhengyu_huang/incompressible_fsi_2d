import numpy as np
import sys
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as plt

sys.path.insert(0, '../Apps')

from IncompressibleFluid import IncompressibleFluid
from MeshGen import mesh_gen
import copy

def steady_cavity():
    ''' A verification case
    Ghia, U.K.N.G., Kirti N.Ghia, and C.T.Shin.
    "High-Re solutions for incompressible flow using the Navier-Stokes equations and a multigrid method."
    Journal of computational physics 48.3(1982): 387 - 411.

    At the vertical line through geometric center of Cavity (Re = 100)
    U = [1,0.84123,0.78871,0.73722,0.68717,0.23151,0.00332,-0.13641,-0.20581,-0.21090,-0.15662,-0.10150,-0.06434,-0.04775,-0.04192,-0.03717,0.0]
    Y = [1.0,0.9766,0.9688,0.9609,0.9531,0.8516,0.7344,0.6172,0.5000,0.4531,0.2813,0.1719,0.1016,0.0703,0.0625,0.0547,0.0]

    At the horizontal line through geometric center of Cavity (Re = 100)
    V = [0.0,-0.05906,-0.07391,-0.08864,-0.10313,-0.16914,-0.22445,-0.24533,0.05454,0.17527,0.17507,0.16077,0.12317,0.10890,0.10091,0.09233,0.0]
    X = [1.0,0.9688,0.9609,0.9531,0.9453,0.9063,0.8594,0.8047,0.50,0.2344,0.2266,0.1563,0.0938,0.0781,0.0703,0.0625,0.0]
    '''

    Nx_ = 32
    ngp_ = 3
    rho_f, nu_f = 1.,  0.01
    Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray, nElements, nNodes, nEquations = mesh_gen(
        'Cavity', Nx_, Nx_, ngp_)
    app0 = IncompressibleFluid(Nx, Ny, IEN, EBC, g, NBC, ID, Coord, problemType, elementType, ngp, nNodesElement, nDofsArray,
                               nElements, nNodes, nEquations, rho_f, nu_f)
    wettedNodesF = np.arange(0, 2 * Nx + 1, dtype=int)
    wettedEdgesF = np.zeros((Nx, 2), dtype=int)
    wettedEdgesF[:, 0] = np.arange(Nx)
    app0.set_wetted_surface(wettedNodesF, wettedEdgesF)

    U0 = np.zeros(app0.nEquations)
    C0 = np.zeros(4 * app0.nNodes)

    U = copy.copy(U0)

    MAXITE = 100
    eps = 1.e-6
    for ite in range(MAXITE):
        K,L = app0.assembly(U, C0)
        dU = -spsolve(K, L)
        res = np.linalg.norm(L)
        print('Ite = ', ite, '||F|| = ', res )
        if(res < eps):
            break
        U += dU

    app0.visualize(U, C0, 0)
    U_all = app0.get_full_field(U, C0) #This case no mesh motion, g = 1

    u_sol = U_all[Nx::2*Nx+1, 0]
    y_sol = Coord[1, Nx::2*Nx+1]
    u_ref = [1, 0.84123, 0.78871, 0.73722, 0.68717, 0.23151, 0.00332, -0.13641, -0.20581, -0.21090, -0.15662, -0.10150,
         -0.06434, -0.04775, -0.04192, -0.03717, 0.0]
    y_ref = [1.0, 0.9766, 0.9688, 0.9609, 0.9531, 0.8516, 0.7344, 0.6172, 0.5000, 0.4531, 0.2813, 0.1719, 0.1016, 0.0703,
         0.0625, 0.0547, 0.0]

    v_sol = U_all[(2*Nx+1)*Ny:(2*Nx+1)*(Ny+1), 1]
    x_sol = Coord[0, (2*Nx+1)*Ny:(2*Nx+1)*(Ny+1)]
    v_ref = [0.0, -0.05906, -0.07391, -0.08864, -0.10313, -0.16914, -0.22445, -0.24533, 0.05454, 0.17527, 0.17507, 0.16077,
         0.12317, 0.10890, 0.10091, 0.09233, 0.0]
    x_ref = [1.0, 0.9688, 0.9609, 0.9531, 0.9453, 0.9063, 0.8594, 0.8047, 0.50, 0.2344, 0.2266, 0.1563, 0.0938, 0.0781,
         0.0703, 0.0625, 0.0]

    plt.figure(1)
    plt.plot(y_sol, u_sol, 'ro', label = 'Q2Q1')
    plt.plot(y_ref, u_ref, 'bo', label = 'Ref')

    plt.figure(2)
    plt.plot(x_sol, v_sol, 'ro', label = 'Q2Q1')
    plt.plot(x_ref, v_ref, 'bo', label = 'Ref')

    plt.legend()
    plt.show()


if __name__=='__main__':
    steady_cavity()