﻿# Welcome to Incompressible_FSI_2D!

Daniel Zhengyu Huang (hzyhzy2014@gmail.com)
Institute for Computational and Mathematical Engineering, Stanford University


This is an educational code for solving fluid structure interaction (FSI) problems with a partitioned spectral deferred correction solver [1]. This document gives implementation details for the modified cavity problem with a flexible bottom in [1]. This problem has been used as a benchmark problem for a variety of FSI studies [2]. An oscillating velocity $\bm{v}(t) = (0, 1 − \cos(2\pi t/5))$ is imposed on the top of the cavity. Each side is of length 1 containing three elements (two unconstrained nodes) that allow free inflow and outflow of fluid, i.e. homogeneous Neumann boundary conditions are imposed on these apertures. 


# Basic solvers
This section will introduce several basic finite element based single physics solvers, including a nonlinear beam element solver for the structure, a linear structure solver for the mesh motion and an incompressible Navier-Stokes solver.

## Nonlinear beam element solver
### Governing equations
The flexible bottom, with fixed ends, is modeled as the nonlinear beam written in the Lagrangian form in the undeformed domain $\Omega_0^s$. The equilibrium equation of the beam in the weak form can be written as [4, p. 308]
$$
\begin{aligned}
\int_{\Omega_0^s} \rho_0^s \ddot{\bm{d}}^s \delta \bm{d}^s dX  + \int_{\Omega_0^s} \sigma_{11}^{s} \delta \epsilon_{11}^{s}dX = \bm{f}_{ext}\delta\bm{d}^s
\end{aligned}
$$
Here $\rho_0^s$ denotes the density, $\bm{d}^s = (u, w)$ denotes the beam horizontal and vertical displacements. The first term is the contribution of the inertial forces, the second and third terms represent the virtual work of the internal forces and the external forces $\bm{f}_{ext}$. $\sigma_{11}^s$ and $\epsilon_{11}^s$ are nonlinear axial strain and axial stress components. The Green-Lagrange strain tensor $\epsilon^s = \frac{1}{2}(F^TF - I)$, here $F$ is the deformation gradient.  The axial strain can be expressed as:
$$\epsilon_{11}^s = \frac{\partial u}{\partial x} + \frac{1}{2}\Big(\frac{\partial u}{\partial x}\Big)^2 + \frac{1}{2}\Big(\frac{d w}{d x}\Big)^2 \approx \frac{\partial u}{\partial x} + \frac{1}{2}\Big(\frac{d w}{d x}\Big)^2$$

With the Kirchhoff assumption: the plane cross-sections remain plane, which holds for thin beams. And let $u_l$ denote the axial displacement at the center line of the beam, the displacement in the $x$ direction is given by 
$$u = u_l - z_l \frac{dw}{dx}$$
here $z_l$ denotes the vertical position in the undeformed configuration with respect to the center line. Let $\chi = -\frac{d^2 w}{dx^2}$ be the curvature, and $\epsilon_l^s =\frac{d u_l}{dx} + \frac{1}{2}\Big(\frac{d w}{d x}\Big)^2$ be the membrane strain, the virtual work of the internal forces can be rewritten as 
$$
\begin{aligned}
\int_{\Omega_0^s} \sigma_{11}^{s} \delta \epsilon_{11}^{s}dX 
&= \int_{\Omega_0^s}  \sigma_{11}^{s} \delta \epsilon_{l}^s + \sigma_{11}^{s} z_l \delta \chi dX\\
&= \int_{l_0^s}  \int_{-t/2}^{t/2} \sigma_{11}^{s} \delta \epsilon_{l}^s + \sigma_{11}^{s} z_l \delta \chi dX\\
&= \int_{l_0^s}  N  \delta \epsilon_{l}^s  +  M\delta \chi dl\\
\end{aligned}
$$
here $t$ is the thickness of the beam, $N = \int_{-t/2}^{t/2} \sigma_{11}^{s} dz_l$ and $M= \int_{-t/2}^{t/2} \sigma_{11}^{s} z_l dz_l$  are normal force and bending moment. which are determined by the linear elasticity constitutive relation.
$$ N = E t\epsilon_l^s \textrm{  and  } M = EI \chi$$
here $E$ is the Young's modulus, $I = \frac{t^3}{12}$ is the moment of inertia of the beam.

The inertial term is written as follows, 
$$
\begin{aligned}
\int_{\Omega_0^s} \rho_0^s \ddot{\bm{d}}^s \delta \bm{d}^s dX  &= \int_{l^s_0}\int_{-t/2}^{t/2}  \rho_0^s  \Big(\big(\ddot{u_l} - z_l\frac{d\ddot{w}}{dx}\big) \big(\delta u_l - z_l\frac{d \delta w}{dx}\big)  + \ddot{w} \delta w \Big)dX\\
 &= \int_{l^s_0}  \rho_0^s  \Big( t \ddot{u_l} \delta u_l +  I \frac{d\ddot{w}}{dx} \frac{d \delta w}{dx}  + t \ddot{w} \delta w \Big)dl
\end{aligned}
$$


### Spatial discretization
The structure is discretized by beam elements with linear shape functions for the horizontal displacement (axial displacement) 
$$\bm{h}_u^T = \Big(\frac{1 - \xi}{2}, \frac{1 - \xi}{2}\Big)$$
and cubic, Hermitian shape functions for the vertical displacement
$$\bm{h}_w^T = \Big(\frac{4 - 6\xi+2\xi^3}{8}, \frac{4 + 6\xi-2\xi^3}{8}, \frac{l_0(\xi^2-1)(\xi-1)}{8}, \frac{l_0(\xi^2-1)(\xi+1)}{8}\Big)$$
here the isoparametric element is $\xi\in[-1, 1]$, and $l_0$ is the element size.
The approximation in the element $e$ is 
$$u_l = \bm{h}_{u}^T \bm{a}^e \textrm{ and } w = \bm{h}_{w}^T \bm{w}^e$$
The superscript $e$ is dropped, when there is no change of ambiguity. 

The inertial term is discretized as follows,

$$
\begin{aligned}
&\int_{l^s_0}  \rho_0^s  \Big( t \ddot{u_l} \delta u_l +  I \frac{d\ddot{w}}{dx} \frac{d \delta w}{dx}  + t \ddot{w} \delta w \Big)dl \\
&= \sum_{e}\int_{e}  \rho_0^s  \Big( t \ddot{u_l} \delta u_l +  I \frac{d\ddot{w}}{dx} \frac{d \delta w}{dx}  + t \ddot{w} \delta w \Big)dl \\
&= \sum_{e}\int_{e} \ddot{\bm{a}}^T\big( \rho_0^s  t \bm{h}_u \cdot \bm{h}_u^T \big)\delta\bm{a} +  \ddot{\bm{w}}^T\big( \rho_0^s  I \frac{d\bm{h}_w}{dx} \cdot  \frac{d\bm{h}_w}{dx}^T  + \rho_0^s  t  \bm{h}_w \cdot \bm{h}_w^T \big)\delta\bm{w}de 
\\
&= \sum_{e}\int_{-1}^{1} \ddot{\bm{a}}^T\big( \rho_0^s  t \bm{h}_u \cdot \bm{h}_u^T \big)\delta\bm{a} +  \ddot{\bm{w}}^T\big( \rho_0^s  I \frac{d\bm{h}_w}{dx} \cdot  \frac{d\bm{h}_w}{dx}^T  + \rho_0^s  t  \bm{h}_w \cdot \bm{h}_w^T \big)\delta\bm{w}\frac{l_0}{2}d\xi \\
\end{aligned}
$$
The jacobian of the isoparametric map is $\frac{dx}{d\xi} = \frac{l_0}{2}$ and  its inverse is $\frac{d\xi}{dx} = \frac{2}{l_0}$. Finally, we have the local mass matrix
$$
\begin{aligned}
\bm{M}_e^s = 
\rho^s_0\begin{bmatrix}  
 t \bm{h}_u \cdot \bm{h}_u^T  & 0 \\  
0 &  I \frac{d\bm{h}_w}{dx} \cdot  \frac{d\bm{h}_w}{dx}^T  +  t  \bm{h}_w \cdot \bm{h}_w^T  
\end{bmatrix} \frac{l_0}{2}
\end{aligned}
$$
which can be assemble to the global mass matrix $\bm{M}^s$ to the corresponding components.

The virtual work of the internal forces is discretized as follows,
$$
\begin{aligned}
&\int_{l_0^s}  N  \delta \epsilon_{l}^s  +  M\delta \chi dl\\
&= \sum_{e}\int_{e}  E t  \epsilon_{l}^s  \delta \epsilon_{l}^s  + E I \chi \delta \chi dl \\
&= \sum_{e}\int_{e}  E t  \Big(\frac{d u_l}{dx} + \frac{1}{2}\big(\frac{d w}{d x}\big)^2\Big)\Big(\frac{d \delta u_l}{dx} + \frac{d w}{d x}\frac{d \delta w}{d x}\Big) + E I \frac{d^2 w}{dx^2} \frac{d^2 \delta w}{dx^2} dl \\
&= \sum_{e}\int_{e}  E t  \Big(\bm{a}^T\frac{d \bm{h}_u}{dx} + \frac{1}{2}\big(\bm{w}^T\frac{d \bm{h}_w}{d x}\big)^2\Big)\Big(\frac{d  \bm{h}_u^T}{dx}\delta\bm{a} + \big(\bm{w}^T\frac{d \bm{h}_w}{d x}\big)\frac{d \bm{h}_w^T}{d x}\delta\bm{w}\Big) + E I \bm{w}^T\frac{d^2 \bm{h}_w}{dx^2} \frac{d^2 \bm{h}_w^T}{dx^2} \delta \bm{w}dl \\
&= \sum_{e}\int_{-1}^{1}  E t  \Big(\bm{a}^T\frac{d \bm{h}_u}{dx} + \frac{1}{2}\big(\bm{w}^T\frac{d \bm{h}_w}{d x}\big)^2\Big)\frac{d  \bm{h}_u^T}{dx}\delta\bm{a}\frac{l_0}{2}  \\
&\qquad + \Big(Et \big(\bm{a}^T\frac{d \bm{h}_u}{dx} + \frac{1}{2}\big(\bm{w}^T\frac{d \bm{h}_w}{d x}\big)^2\big) \big(\bm{w}^T\frac{d \bm{h}_w}{d x}\big)\frac{d \bm{h}_w^T}{d x} + E I \bm{w}^T\frac{d^2 \bm{h}_w}{dx^2} \frac{d^2 \bm{h}_w^T}{dx^2} \Big)\delta \bm{w}\frac{l_0}{2}d\xi \\
\end{aligned}
$$
Therefore, the local internal force vector and stiff matrix are  
$$
\begin{aligned}
\bm{f}_e^s = 
\begin{bmatrix}  
 N  \frac{d  \bm{h}_u}{dx} \\  
 N  (\bm{w}^T\frac{d \bm{h}_w}{dx})\frac{d\bm{h}_w}{dx} - M\frac{d^2\bm{h}_w}{dx}
\end{bmatrix} \frac{l_0}{2}
\end{aligned}
$$
$$
\begin{aligned}
\bm{K}_e^s = 
\begin{bmatrix}  
Et \frac{d \bm{h}_u}{dx} \cdot \frac{d \bm{h}_u^T}{dx} & Et  (\bm{w}^T\frac{d \bm{h}_w}{d x})\frac{d \bm{h}_u}{d x}\cdot \frac{d  \bm{h}_w^T}{dx} \\  
~\\
 Et(\bm{w}^T\frac{d\bm{h}_w}{d x}) \frac{d\bm{h}_w}{dx}\cdot \frac{d\bm{h}_u^T }{dx}
 & \Big(N + Et\big(\bm{w}^T\frac{d \bm{h}_w}{d x}\big)^2\Big)  \frac{d \bm{h}_w}{d x}\cdot\frac{d \bm{h}_w^T}{d x} +  EI\frac{d^2\bm{h}_w}{dx^2} \cdot \frac{d^2\bm{h}_w^T}{dx^2}
\end{bmatrix} \frac{l_0}{2}
\end{aligned}
$$
This leads to a second order ODE system
$$\bm{M}^s \ddot{\bm{d}}^s + \bm{F}(\bm{d}^s) = \bm{F}^{ext}$$
## Linear structure solver
### Governing equations
The mesh motion is modeled as quasi-structure, and the governing equations for a linear structure are 
 $$
 \begin{aligned}
 \rho_0^{x}\ddot{\bm{d}}^x - \nabla : \bm{\sigma}^x &= 0
 \qquad\qquad &&\text{in}~\Omega^x, \\
 \bm{d}^x &= \bm{d}_D(t)
 \qquad\qquad &&\text{on}~\partial \Omega^x_D, \\
 \dot{\bm{d}}^x &= \dot{\bm{d}}_D(t)
 \qquad\qquad &&\text{on}~\partial \Omega^x_D,
 \end{aligned}
 $$
where $\rho^x$ is the density, and $\bm{\sigma}^{x}$ is the Cauchy stress tensor with for example linear plane strain assumption. 
$$
\begin{aligned}
\begin{bmatrix}
\bm{\sigma}^{x}_{11} \\
\bm{\sigma}^{x}_{22} \\
\bm{\sigma}^{x}_{12} 
\end{bmatrix}=  
H
\begin{bmatrix}
\bm{\epsilon}^{x}_{11} \\
\bm{\epsilon}^{x}_{22} \\
2\bm{\epsilon}^{x}_{12} 
\end{bmatrix} 
\textrm{ and }
 H = 
 \frac{E}{(1 +\nu)(1 - 2\nu)}
\begin{bmatrix}
1 - \nu & \nu & 0\\
 \nu & 1 - \nu & 0\\
 0&0& (1 - 2\nu )/2\\
\end{bmatrix} 
\end{aligned}
$$

The position and velocity of the fluid domain are prescribed along $\partial \Omega^x_D$, the union of the fluid-structure interface and the fluid domain boundary. 

Let $\delta \bm{d}$ be the trail test function, the weak form is written as follows
$$
\begin{aligned}
  \int_{\Omega^x} \rho_o^s \ddot{\bm{d}}^x \cdot  \delta  \bm{d} dX &= \int_{\Omega^x} \nabla: \bm{\sigma}^x \cdot \delta \bm{d}dX = \int_{\Omega^x} -\bm{\sigma}^x: \nabla \delta\bm{d} dX 
\end{aligned}                                                  
$$

### Spatial discretization

The governing equation is semi-discretized by the continuous biquadratic element to match the fluid semi-discretization
$$
\begin{matrix}
4   &  7 &    3\\
8    & 9    & 6\\
1    & 5  &   2\\
\end{matrix}
$$
$$
\begin{aligned}
\bm{h}^T = \Big(&\frac{\xi_1(\xi_1 - 1)\xi_2 (\xi_2 - 1)}{4.0}, 
                                      \frac{\xi_1 (\xi_1 + 1) \xi_2 (\xi_2 - 1)}{4.0}, 
                                      \frac{\xi_1(\xi_1 + 1) \xi_2 (\xi_2 + 1)}{4.0}, 
                                      \frac{\xi_1 (\xi_1 - 1) \xi_2(\xi_2 + 1)}{4.0}, \\  
                                     &-\frac{(\xi_1^2 - 1) \xi_2 (\xi_2 - 1.)}{2.0}, 
                                     -\frac{\xi_1 (\xi_1 + 1)(\xi_2^2 - 1.)}{2.0}, 
                                     -\frac{(\xi_1^2 - 1) \xi_2(\xi_2 + 1.)}{2.0}, 
                                     -\frac{\xi_1 (\xi_1 - 1)(\xi_2^2- 1.)}{2.0},\\ 
                                     &(1 - \xi_1^2) (1 - \xi_2^2)\Big).
\end{aligned}
$$
here the isoparametric element is $\xi_1 \times \xi_2\in[-1, 1]\times [−1,1]$, with the Jacobian and determinant $G$ and $g$. The approximation in each element is 
$$d_x = \bm{h}^T \bm{d}_x \textrm{ and } d_y=\bm{h}^T\bm{d}_y$$

The inertial term is discretized as follows, 
 $$
 \begin{aligned}
 \int_{V_0}\rho_0^x \ddot {\bm{d}}^x \cdot \delta\bm{d} dX 
 &= \sum_e \rho_0^x \int_e \ddot{\bm{d}}_x^T\bm{h}\cdot \bm{h}^T\delta\bm{d}_x + \ddot{\bm{d}}_y^T\bm{h} \cdot \bm{h}^T\delta\bm{d}_y dX \\
&= \sum_e \rho_0^x \int_{\xi_1}\int_{\xi_2} \Big(\ddot{\bm{d}}_x^T\bm{h}\cdot \bm{h}^T\delta\bm{d}_x + \ddot{\bm{d}}_y^T\bm{h}\cdot \bm{h}^T\delta\bm{d}_y\Big) g d\xi_1d\xi_2 
\end{aligned}
$$
The local mass matrix is 
$$
\begin{aligned}
\bm{M}_e^x = \rho_0^x\begin{bmatrix}  
 \bm{h}\cdot \bm{h}^T  & 0 \\  
0 & \bm{h}\cdot \bm{h}^T \\
\end{bmatrix} g
\end{aligned}
$$
which can be assembled to the global mass matrix $\bm{M}^x$ to the corresponding components.

The virtual work of the internal forces is discretized as follows,
$$
\begin{aligned}
&\int_{\Omega^x} \bm{\sigma}^x: \nabla \delta\bm{d} dX dS \\
= &\int_{\Omega^x}
\begin{bmatrix}
\frac{\partial \bm{h}}{\partial x}^T\bm{d}_x& \frac{\partial \bm{h}}{\partial y}^T\bm{d}_y & \frac{\partial \bm{h}}{\partial x}^T\bm{d}_y + \frac{\partial \bm{h}}{\partial y}^T\bm{d}_x 
\end{bmatrix}
 H^T 
 \begin{bmatrix}
\frac{\partial \bm{h}}{\partial x}^T\delta\bm{d}_x\\ 
\frac{\partial \bm{h}}{\partial y}^T\delta\bm{d}_y \\ 
\frac{\partial \bm{h}}{\partial x}^T\delta\bm{d}_y + \frac{\partial \bm{h}}{ \partial y}^T\delta\bm{d}_x 
\end{bmatrix} dX \\
= &\int_{\Omega^x}
\begin{bmatrix}
\bm{d}_x^T& \bm{d}_y^T 
\end{bmatrix}
\begin{bmatrix}
\frac{\partial \bm{h}}{\partial x} & 0 & \frac{\partial \bm{h}}{\partial y}\\
0 & \frac{\partial \bm{h}}{\partial y}  & \frac{\partial \bm{h}}{\partial x}\\
\end{bmatrix}
 H^T 
 \begin{bmatrix}
\frac{\partial \bm{h}}{\partial x}^T & 0 \\ 
0 &\frac{\partial \bm{h}}{\partial y}^T \\ 
\frac{\partial \bm{h}}{\partial y}^T & \frac{\partial \bm{h}}{ \partial x}^T 
\end{bmatrix}
 \begin{bmatrix}
 \delta\bm{d}_x\\ 
 \delta\bm{d}_y
\end{bmatrix} dX 
\end{aligned}                                                  
$$
The local stiffness matrix is 
$$
\begin{aligned}
\bm{K}_e^x = 
\begin{bmatrix}
\frac{\partial \bm{h}}{\partial x} & 0 & \frac{\partial \bm{h}}{\partial y}\\
0 & \frac{\partial \bm{h}}{\partial y}  & \frac{\partial \bm{h}}{\partial x}\\
\end{bmatrix}
 H^T 
 \begin{bmatrix}
\frac{\partial \bm{h}}{\partial x}^T & 0 \\ 
0 &\frac{\partial \bm{h}}{\partial y}^T \\ 
\frac{\partial \bm{h}}{\partial y}^T & \frac{\partial \bm{h}}{ \partial x}^T 
\end{bmatrix}
\end{aligned}                                                  
$$
which leads to a linear ODE system,
$$\bm{M}^x \ddot{\bm{d}}^x + \bm{K}^x \cdot \bm{d}^x = 0$$

## Incompressible Navier-Stokes solver
### Governing equations
The conservation laws, i.e. mass conservation and momentum conservation, in any deformed control volume $V_t$ are written as 
$$
\frac{d}{dt}\int_{V_t} \rho^f dx + \int_{\partial V_t}\rho^f\big(\bm{v} - \dot{\bm{d}}^x\big)\cdot\bm{n} ds= 0\\
~\\
\frac{d}{dt}\int_{V_t} \rho^f \bm{v}dx + \int_{\partial V_t}\rho^f\bm{v}\big((\bm{v} - \dot{\bm{d}}^x)\cdot\bm{n}\big)ds -  \int_{\partial V_t}\bm{\sigma^f}\cdot\bm{n}ds = 0\\
$$
Here $\rho^f$ is the constant fluid density (incompressible flow), and $\bm{v}$ and $\dot{\bm{d}}^x$ are the flow velocity and mesh velocity, and  $\bm{\sigma}^f$ is the stress tensor
$$\bm{\sigma}^f = -pI + \mu\big(\nabla\bm{v} + \nabla\bm{v}^T\big)$$

The map of the deformed domain or mesh motion from $V_0$ to $V_t$ is 
$$(x,y) = \bm{d}^x(X, Y, t)$$
with 
$$\dot{\bm{d}}^x = \frac{\partial \bm{d}^x}{\partial t} \quad G = \nabla_X \bm{d}^x \quad g = \det{G}$$
This leads to the geometric conservation relation
$$
\begin{aligned}
\frac{d}{dt}\int_{V_0}  g dX  = \frac{d}{dt}\int_{V_t}  1 dx  =  \int_{\partial V_t} \dot{\bm{d}}^x \cdot \bm{n} ds= \int_{V_t} \nabla_x \cdot  \dot{\bm{d}}^x  dx= \int_{V_0} (\nabla_x \cdot  \dot{\bm{d}}^x)  g dX.
\end{aligned}
$$
Bringing the geometric conservation relation into the mass conservation equation leads to, 
$$
\begin{aligned}
0&=\frac{d}{dt}\int_{V_t} \rho^f dx + \int_{\partial V_t}\rho^f\big(\bm{v} - \dot{\bm{d}}^x\big)\cdot\bm{n} ds\\
~\\
&= \int_{\partial V_t}\rho^f \bm{v}\cdot\bm{n} ds = \int_{V_t}\rho^f \nabla_x\cdot\bm{v} dx = \rho^f\int_{V_0} \nabla_x\cdot\bm{v} g dx\\
\end{aligned}
$$
which leads to the strong form
$$
\nabla_x\cdot\bm{v} = 0\\
$$
Bringing the geometric conservation relation into the momentum conservation equation leads to
$$
\begin{aligned}
0 &= \frac{d}{dt}\int_{V_t} \bm{v}dx + \int_{\partial V_t}\bm{v}\big((\bm{v} - \dot{\bm{d}}^x)\cdot\bm{n}\big)ds -  \int_{V_t}\nabla_x :\frac{ \bm{\sigma^f}}{\rho^f}dx\\
~\\
&= \frac{d}{dt}\int_{V_0} \bm{v} gdX + \int_{V_t}\nabla_x : \big(\bm{v} \cdot (\bm{v} - \cdot{\bm{d}}^x)^T\big)dx -  \int_{V_t}\nabla_x :\frac{ \bm{\sigma^f}}{\rho^f}dx\\
~\\
&= \int_{V_0} \frac{d \bm{v}}{dt}  gdX + \int_{V_0} \frac{d g}{dt}  \bm{v}dX +\int_{V_t}\nabla_x :\big(\bm{v} \cdot (\bm{v} - \dot{\bm{d}}^x)^T\big)dx -  \int_{V_t}\nabla_x :\frac{ \bm{\sigma^f}}{\rho^f}dx\\
~\\
&= \int_{V_0} \frac{d \bm{v}}{dt} gdX + \int_{V_0} (\nabla_x \cdot  \dot{\bm{d}}^x) \bm{v}gdX +\int_{V_t}\nabla_x :\big(\bm{v} \cdot (\bm{v} - \dot{\bm{d}}^x)^T\big)dx -  \int_{V_t}\nabla_x :\frac{ \bm{\sigma^f}}{\rho^f}dx\\
~\\
&= \int_{V_t} \frac{d \bm{v}}{dt} dx + \int_{V_t} (\bm{v} - \dot{\bm{d}}^x) : \nabla_x \bm{v}dx -  \int_{V_t}\nabla_x :\frac{ \bm{\sigma^f}}{\rho^f}dx\\
~\\
&= \int_{V_0} \frac{d \bm{v}}{dt} gdX + \int_{V_0} (\bm{v} - \dot{\bm{d}}^x) : \nabla_x \bm{v} gdX -  \int_{V_0}\nabla_x :\frac{ \bm{\sigma^f}}{\rho^f} gdX\\
\end{aligned}
$$
which leads to the strong form on the undeformed domain 
$$
 \begin{aligned}
\frac{\partial \bm{v}}{\partial t} + ( \bm{v} - \dot{\bm{d}}^x) : \nabla_x \bm{v} - \nabla_x :  \frac{\bm{\sigma^f}}{\rho^f} &= 0 \qquad&&\textrm{in}~\Omega_0^f, \\
\nabla_x  \cdot  \bm{v} &= 0 \qquad&&\textrm{in}~\Omega_0^f,\\
\bm{v}(t) &= \bm{v}_D(t) \qquad&&\textrm{in}~\Gamma^f_{0D}\\
\bm{\sigma}^f \cdot \bm{N} &= \bm{t_0} &\qquad&\textrm{in}~\Gamma^f_{0N}.\\
 \end{aligned}
 $$
 
Let $\delta\bm{v}$ and $\delta p$ be the trail test functions for the velocity and pressure, respectively. The weak form is written as follows,
 $$
 \begin{aligned}
0 &= \int_{\Omega^f_0}\frac{\partial \bm{v}}{\partial t} \cdot \delta\bm{v} + \Big(( \bm{v} - \dot{\bm{d}}^x) : \nabla_x \bm{v}\Big)\cdot\delta\bm{v} - \Big(\nabla_x :  \frac{\bm{\sigma^f}}{\rho^f}\Big)\cdot \delta\bm{v} dX + \int_{\Omega^f_0} \Big(\nabla_x  \cdot  \bm{v} \Big) \delta p dX\\
~\\
&= \int_{\Omega^f_0}\frac{\partial \bm{v}}{\partial t} \cdot \delta\bm{v} + \Big(( \bm{v} - \dot{\bm{d}}^x) : \nabla_x \bm{v}\Big)\cdot\delta\bm{v} + \frac{\bm{\sigma^f}}{\rho^f} : \nabla_x \delta\bm{v} dX - \int_{\partial \Gamma^f_{0N}}  \bm{t}_0 \cdot \delta\bm{v}dS + \int_{\Omega^f_0} \Big(\nabla_x  \cdot  \bm{v} \Big)  \delta p dX\\
 \end{aligned}
 $$

### Spatial discretization

The governing equations in is semi-discretized traditional Taylor-Hood Q2Q1 mixed elements, i.e.
continuous biquadratic velocity 
$$
\begin{matrix}
4   &  7 &    3\\
8    & 9    & 6\\
1    & 5  &   2\\
\end{matrix}
$$
$$
\begin{aligned}
\bm{h}_v^T = \Big(&\frac{\xi_1(\xi_1 - 1)\xi_2 (\xi_2 - 1)}{4.0}, 
                                      \frac{\xi_1 (\xi_1 + 1) \xi_2 (\xi_2 - 1)}{4.0}, 
                                      \frac{\xi_1(\xi_1 + 1) \xi_2 (\xi_2 + 1)}{4.0}, 
                                      \frac{\xi_1 (\xi_1 - 1) \xi_2(\xi_2 + 1)}{4.0}, \\  
                                     &-\frac{(\xi_1^2 - 1) \xi_2 (\xi_2 - 1.)}{2.0}, 
                                     -\frac{\xi_1 (\xi_1 + 1)(\xi_2^2 - 1.)}{2.0}, 
                                     -\frac{(\xi_1^2 - 1) \xi_2(\xi_2 + 1.)}{2.0}, 
                                     -\frac{\xi_1 (\xi_1 - 1)(\xi_2^2- 1.)}{2.0},\\ 
                                     &(1 - \xi_1^2) (1 - \xi_2^2)\Big)
\end{aligned}
$$
and continuous bilinear pressure, 
$$
\begin{matrix}
4   &    &    3\\
    &     & \\
1    &   &   2\\
\end{matrix}
$$
$$\bm{h}_p^T = \Big(\frac{(\xi_1 - 1)(\xi_2 - 1)}{4}, -\frac{(\xi_1 + 1)(\xi_2 - 1)}{4}, \frac{(\xi_1 + 1)(\xi_2 + 1)}{4}, -\frac{(\xi_1 - 1)(\xi_2 + 1)}{4}\Big)$$
which satisfies the Babuška-Brezzi condition [3, p. 286]. Here the isoparametric element is $\xi_1\times\xi_2\in[-1, 1]\times[-1, 1]$, with the Jacobian and determinant $G$ and $g$ evaluated by using the Q2 element. 

The approximation in each element is 
$$v_x = \bm{h}_{v}^T \bm{a_x} \textrm{ and } v_y=\bm{h}_v^T\bm{a}_y\textrm{ and }p = \bm{h}_{p}^T \bm{p}$$

The inertial term is discretized as follows, 
 $$
 \begin{aligned}
\int_{V_0}\frac{\partial \bm{v}}{\partial t} \cdot \delta\bm{w} dX = &\sum_e\int_e \bm{a}_x^T\bm{h}_v \cdot \bm{h}_v^T\delta\bm{a}_x + \bm{a}_y^T\bm{h}_v \cdot \bm{h}_v^T\delta\bm{a}_y dX\\
= &\sum_e\int_{\xi_1}\int_{\xi_2} \Big(\bm{a}_x^T\bm{h}_v \cdot \bm{h}_v^T\delta\bm{a}_x + \bm{a}_y^T\bm{h}_v \cdot \bm{h}_v^T\delta\bm{a}_y \Big)gd\xi_1d\xi_2
\end{aligned}
$$
The local mass matrix is 
$$
\begin{aligned}
\bm{M}_e^f = \begin{bmatrix}  
 \bm{h}_v \cdot \bm{h}_v^T  & 0  & 0\\  
0 & \bm{h}_v \cdot \bm{h}_v^T & 0 \\
0&0&0  
\end{bmatrix}g
\end{aligned}
$$
which can be assembled to the global mass matrix $\bm{M}^f$ to the corresponding components, it is worth mentioning that the mass matrix is singular due to the incompressible constraint.

The other terms are discretized as follows,  
$$
 \begin{aligned}
&\int_{V_0} \Big(( \bm{v} - \dot{\bm{d}}^x) : \nabla_x \bm{v}\Big)\cdot\delta\bm{v} \\
=&\int_{V_0} 
\begin{bmatrix}
(\bm{a}_x -  \dot{\bm{d}}_x)^T\bm{h}_v\cdot\frac{\partial \bm{h}_v}{\partial x}^T\bm{a}_x  +  (\bm{a}_y -  \dot{\bm{d}}_y)^T\bm{h}_v\cdot\frac{\partial \bm{h}_v}{\partial y}^T\bm{a}_x\\
(\bm{a}_x -  \dot{\bm{d}}_x)^T\bm{h}_v\cdot\frac{\partial \bm{h}_v}{\partial x}^T\bm{a}_y  +  (\bm{a}_y -  \dot{\bm{d}}_y)^T\bm{h}_v\cdot\frac{\partial \bm{h}_v}{\partial y}^T\bm{a}_y
\end{bmatrix}^T
\cdot
 \begin{bmatrix}
 \bm{h}_v^T \delta \bm{a}_x\\
  \bm{h}_v^T \delta \bm{a}_y\\
 \end{bmatrix} 
\end{aligned}
$$
And
$$
\begin{aligned}
&\int_{V_0}\frac{\bm{\sigma^f}}{\rho^f} : \nabla_x \delta\bm{v} dX \\
=& \frac{1}{\rho^f}\int_{V_0} 
\begin{bmatrix}
-\bm{h}_p^T\bm{p}  + 2\mu\frac{\partial \bm{h}^T_v}{\partial x}\bm{a}_x& \mu\frac{\partial \bm{h}^T_v}{\partial x}\bm{a}_y + \mu\frac{\partial \bm{h}^T_v}{\partial y}\bm{a}_x\\
~\\
\mu\frac{\partial \bm{h}^T_v}{\partial x}\bm{a}_y + \mu\frac{\partial \bm{h}^T_v}{\partial y}\bm{a}_x & -\bm{h}_p^T\bm{p}+ 2\mu\frac{\partial \bm{h}^T_v}{\partial y}\bm{a}_y\\
\end{bmatrix}
:
 \begin{bmatrix}
 \frac{\partial\bm{h}^T_v}{\partial x} \delta \bm{a}_x & \frac{\partial\bm{h}^T_v}{\partial y} \delta \bm{a}_x\\
 ~\\
  \frac{\partial\bm{h}^T_v}{\partial x} \delta \bm{a}_y & \frac{\partial\bm{h}^T_v}{\partial y} \delta \bm{a}_y\\
 \end{bmatrix} 
 \end{aligned}
 $$
 And 
 $$
\begin{aligned}
&\int_{V_0} \Big(\nabla_x  \cdot  \bm{v} \Big)  \delta p dX\\
=&\int_{V_0} \Big(\bm{a}_x^T\frac{\partial\bm{h}_v}{\partial x} + \bm{a}_y^T\frac{\partial\bm{h}_v}{\partial y} \Big)  \bm{h}_p^T \delta \bm{p} dX\\
 \end{aligned}
 $$
  And
 $$
\begin{aligned}
\int_{\partial V_0} \bm{t}_0 \cdot \delta\bm{v}  dS = 
\begin{aligned}
\int_{\partial V_0}\bm{t}_{0,x} \bm{h}_u^T \delta \bm{a}_x + \bm{t}_{0,y} \bm{h}_u^T \delta \bm{a}_y dS  
 \end{aligned}
 \end{aligned}
 $$
When the  traction force is $\bm{t}_0=0$, this term vanishes.  Therefore, the local internal force vector and stiffness matrix are
$$
\begin{aligned}
\bm{f}_e^f = 
\begin{bmatrix}
\Big((v_x -  \dot{d}_x)\frac{\partial \bm{h}_v}{\partial x}^T\bm{a}_x  + (v_y -  \dot{d}_y)\frac{\partial \bm{h}_v}{\partial y}^T\bm{a}_x\Big)\bm{h}_v + \frac{\bm{\sigma_{xx}^f}}{\rho^f}\frac{\partial\bm{h}_v}{\partial x} + \frac{\bm{\sigma_{xy}^f}}{\rho^f}\frac{\partial\bm{h}_v}{\partial y}\\
\Big((v_x -  \dot{d}_x)\frac{\partial \bm{h}_v}{\partial x}^T\bm{a}_y  +  (v_y -  \dot{d}_y)\frac{\partial \bm{h}_v}{\partial y}^T\bm{a}_y\Big)\bm{h}_v + \frac{\bm{\sigma_{yx}^f}}{\rho^f}\frac{\partial\bm{h}_v}{\partial x} + \frac{\bm{\sigma_{yy}^f}}{\rho^f}\frac{\partial\bm{h}_v}{\partial y}\\
\Big(\bm{a}_x^T\frac{\partial\bm{h}_v}{\partial x} + \bm{a}_y^T\frac{\partial\bm{h}_v}{\partial y} \Big)  \bm{h}_p
\end{bmatrix} g\\
\end{aligned}
$$

$$
\begin{aligned}
\bm{K}_e^f = 
\begin{bmatrix}
\Big(\frac{\partial v_x}{\partial x}\bm{h}_v\cdot\bm{h}_v^T +(v_x -  \dot{d}_x)\bm{h}_v\cdot\frac{\partial \bm{h}^T_v}{\partial x}  +   (v_y - \dot{d}_y)\bm{h}_v\cdot\frac{\partial \bm{h}^T_v}{\partial y}  + 2\nu\frac{\partial\bm{h}_v}{\partial x}\cdot \frac{\partial\bm{h}^T_v}{\partial x} + \nu\frac{\partial\bm{h}_v}{\partial y}\cdot \frac{\partial\bm{h}^T _v}{\partial y}
& \frac{\partial v_x}{\partial y}\bm{h}_v\cdot\bm{h}_v^T+\nu\frac{\partial \bm{h}_v}{\partial y}\cdot\frac{\partial \bm{h}^T_v}{\partial x}
&-\frac{1}{\rho^f}\frac{\partial \bm{h}^T_v}{\partial x}\bm{h}_p\\
\frac{\partial v_y}{\partial x}\bm{h}_v\cdot\bm{h}_v^T +\nu\frac{\partial\bm{h}_v}{\partial x}\cdot\frac{\partial\bm{h}^T_v}{\partial y}
&\frac{\partial v_y}{\partial y}\bm{h}_v\cdot \bm{h}_v^T + (v_y-\dot{d}_y)\bm{h}_v\cdot \frac{\partial\bm{h}^T_v}{\partial y} +  (v_x -\dot{d}_x)\bm{h}_v\cdot \frac{\partial \bm{h}^T_v}{\partial x} + 2\nu\frac{\partial \bm{h}_v}{\partial y}\cdot \frac{\partial \bm{h}^T_v}{\partial y} + \nu\frac{\partial \bm{h}_v}{\partial x}\cdot \frac{\partial \bm{h}^T_v}{\partial x} 
&-\frac{1}{\rho^f}\frac{\partial \bm{h}^T_v}{\partial y}\bm{h}_p\\
 \bm{h}_p \frac{\partial\bm{h}^T_v}{\partial x}&  \bm{h}_p \frac{\partial\bm{h}^T_v}{\partial y}& 0
\end{bmatrix}g
\end{aligned}
$$
which lead to the nonlinear ODE system
$$\bm{M}^f \dot{\bm{u}}^f + \bm{F}(\bm{u}^f) = 0$$

# Time dependent Dirichlet boundary condition
Time dependent Dirichlet boundary conditions appear at the structure boundary at least in the quasi-structure mesh motion,  or at the fluid boundary due to no-slip boundary condition in the present example.  Some treatments are listed
## Elimination method
The discretized ODEs are written as follows (Use structure equations as an example)
$$
\begin{aligned}
\begin{bmatrix}
M_I & M_{ID}\\
M_{ID}^T & M_{D}
\end{bmatrix}
\begin{bmatrix}
\ddot{\bm{d}}_I\\
\ddot{\bm{d}}_D
\end{bmatrix}
=\begin{bmatrix}
\bm{f}_I\\
\bm{f}_D
\end{bmatrix}
\end{aligned}
$$
Here subscripts $I$ and $D$ indicate interior and time-dependent Dirichlet parts, separately.  
In general, the force $\bm{f}_D$ is unknown. Instead of solving the whole system, we only solve the interior part, and the ODE system becomes 
$$
\begin{aligned}
M_I \ddot{\bm{d}}_I =  \bm{f}_I - M_{ID} \ddot{\bm{d}}_D
\end{aligned}
$$
It is worth mentioning the accelerating of the time-dependent Dirichlet part is required. However, when the mass matrix is diagonal, which is valid for finite volume methods or the lumped mass matrix, this term vanishes due to $M_{ID} = 0$.  In the present implementation, the lumped mass is used for both the fluid and the mesh motion, which might deteriorate the accuracy.
## Weak imposition 
This is generally used in the fluid simulation, especially the finite volume method. The flow velocity is treated as unknowns on the wall or fluid-structure interface instead of the Dirichlet boundary. Therefore, the pressure load and shear force load on these surfaces are computed by using these Dirichlet values, which gives $\bm{f}_D$ in the whole ODE system. And after each time step, the values on the Dirichlet boundary might need to be enforced or clipped to the Dirichlet values. 

# Coupling procedure
This section will introduce the coupling procedure for basic fluid-structure interaction problems. 
For the current case, the discretized fluid domain and discreteized structure domain do not match at the interface, the conservative displacement load transfer algorithm (slave-master kinematics) in [5] is applied

## Node matching
Before the simulation, the meshes are pre-processed. Each node $s_i$ on the interface of the fluid domain is matched to its closest point  $m_i$ on the interface of the structure domain, which is in the structure surface element $e_i$ with local coordinate $\xi_i$.
## Displacement computation
At each time step, the displacement and velocity of node $s_i$ are assumed to be the same as these of node $m_i$, which are interpolated as follows
$$
\begin{aligned}
\bm{d}^x_{s_i} = \bm{d}^s_{m_i} =  \Big(\bm{h}_u^T(\xi_i)\bm{a}^{e_i} , \bm{h}_w^T(\xi_i)\bm{w}^{e_i}  \Big)^T \\ 
\dot{\bm{d}}^x_{s_i}  = \dot{\bm{d}}^s_{m_i} =  \Big(\bm{h}_u^T(\xi_i)\dot{\bm{a}}^{e_i}  , \bm{h}_w^T(\xi_i)\dot{\bm{w}}^{e_i}  \Big)^T \\
\end{aligned}
$$
The displacement $\bm{d}^x$ and velocity $\dot{\bm{d}}^x$ of  fluid nodes at the interface 
$$\bm{d}^x = T_M \bm{d}^s \textrm{ and } \dot{\bm{d}}^x = T_M \dot{\bm{d}}^s$$ 
Here $T_M$ is the interpolation matrix, and $\bm{d}^s$ and $\dot{\bm{d}}^s$ are the structure interface nodal displacement and velocity, for the present case, they contain the whole structure.
## Load computation
Nodal force loads on the fluid interface are computed as follows, 
$$
\begin{aligned}
\bm{f}_i^f = -\int_{\Gamma_{F/S}} \bm{\sigma}^f\cdot \bm{n} \phi_i ds = -\int_{\Gamma_{0,F/S}} \bm{\sigma}^f\cdot (g G^{-T}\bm{N}) \phi_i dS
\end{aligned}
$$
Here $\phi_i$ is the nodal basis function corresponding to the $i$th  component of the force load, and Nanson's relation is applied,
$$
\bm{n} ds = g G^{-T}\bm{N}dS
$$
To guarantee the local force conservation, we need the virtual work to be equal on both fluid side and the structure side, namely
$$\bm{f}^s \cdot \delta \bm{d}^s = \bm{f}^f \cdot \delta \bm{d}^x =  \bm{f}^f \cdot \Big(T_M\delta \bm{d}^x\Big)$$ 
Therefore, the nodal force loads on the structure interface are computed as follows,  
$$\bm{f}^s = \bm{T_M}^T\bm{f}^f $$ 


# References
[1] Daniel Z. Huang, Will Pazner, Per-olof Persson, and Matthew J. Zahr. "High-order partitioned spectral deferred correction solvers for muliphysics problems"

[2] Förster, Christiane, Wolfgang A. Wall, and Ekkehard Ramm. "Artificial added mass instabilities in sequential staggered coupling of nonlinear structures and incompressible viscous flows."
Computer methods in applied mechanics and engineering 196.7 (2007): 1278-1293.

[3] Donea, Jean, and Antonio Huerta. Finite element methods for flow problems. John Wiley & Sons, 2003.

[4] De Borst, René, et al. Nonlinear finite element analysis of solids and structures. John Wiley & Sons, 2012. 

[5] Farhat, Charbel, Michael Lesoinne, and Patrick Le Tallec. "Load and motion transfer algorithms for fluid/structure interaction problems with non-matching discrete interfaces: Momentum and energy conservation, optimal discretization and application to aeroelasticity." _Computer methods in applied mechanics and engineering_ 157.1-2 (1998): 95-114.

# Appendix
Some of the notations used in this document are written in details, for $a , b\in R^2$ and $A, B\in R^{2\times2}$ 
$$
\begin{aligned}
\nabla : A = 
\begin{bmatrix}
\partial_x A_{11} + \partial_y A_{12}\\
\partial_x A_{21} + \partial_y A_{22}\\
\end{bmatrix}
\end{aligned}
$$
$$
\begin{aligned}
A : B = A_{11}B_{11}  + A_{12}B_{12} + A_{21}B_{21}  + A_{22}B_{22}
\end{aligned}
$$
$$
\begin{aligned}
a : A =  A\cdot a^T = \begin{bmatrix}
a_{1}A_{11} +  a_{2}A_{12}\\
a_{1}A_{21} +  a_{2}A_{22}\\
\end{bmatrix}
\end{aligned}
$$
$$
\begin{aligned}
\nabla \cdot a =  \partial_x a_{1} + \partial_y a_{2}
\end{aligned}
$$
$$
\begin{aligned}
\nabla  a 
=  \begin{bmatrix}
\partial_x a_{1} & \partial_y a_{1}\\
\partial_x a_{2} & \partial_y a_{2}\\
\end{bmatrix}
\end{aligned}
$$

$$
\begin{aligned}
a \cdot b = a^T \cdot b = a_1b_1 + a_2 b_2
\end{aligned}
$$


